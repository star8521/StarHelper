﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace StarEF
{
    public interface IEFBaseRepository<T>
    {
        T Add(T entity);

        Task<T> AddAsync(T entity);

        void BatchAdd(IEnumerable<T> entitys);

        Task BatchAddAsync(IEnumerable<T> entitys);

        void Delete(T entity);

        void Delete(object id);

        void Delete(Expression<Func<T, bool>> predicate);

        void Update(T entity);

        bool Exist(Expression<Func<T, bool>> predicate);

        Task<bool> ExistAsync(Expression<Func<T, bool>> predicate);

        Task<int> CountAsync(Expression<Func<T, bool>> predicate);

        int Count(Expression<Func<T, bool>> predicate);

        T Get(Expression<Func<T, bool>> predicate);

        Task<T> GetAsync(Expression<Func<T, bool>> predicate);

        Task<T> GetAsync(object id);

        T Get(object id);

        IQueryable<T> List(Expression<Func<T, bool>> predicate = null);

        IQueryable<T> List<S>(Expression<Func<T, bool>> whereLambda, bool isAsc, Expression<Func<T, S>> orderLambda, string includeProperties = "");

        IQueryable<T> FindListBySSQL(string sql, params object[] parameters);

        IQueryable<S> FromSqlRaw<S>(string sql, params object[] parameters) where S : class;

        int ExcuteBySQL(string sql, params object[] parameters);

        Task<int> ExcuteBySQLAsync(string sql, params object[] parameters);

        void Save();
    }
}