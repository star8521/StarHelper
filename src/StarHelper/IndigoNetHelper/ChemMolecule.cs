﻿using com.epam.indigo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;

namespace IndigoNetHelper
{
    public class ChemMolecule
    {
        private Indigo ido = null;
        private string _molorsmiles = "";

        #region 构造函数
        public ChemMolecule() { }
        public ChemMolecule(Indigo _ido, IndigoObject io)
        {
            ido = _ido;
            _molorsmiles = io.molfile();
            _io = io;
        }
        #endregion

        #region 元素
        private IndigoObject _io = null;
        public IndigoObject IDO
        {
            get { return _io; }
        }
        public string MolFile()
        {
            if (_io != null)
            {
                _io.dearomatize();
                _io.layout();
                return _io.molfile();
            }
            return "";
        }
        public string CanonicalSmiles()
        {
            if (_io != null)
            {
                try
                {
                    var smiles = _io.canonicalSmiles();

                    if (!string.IsNullOrEmpty(smiles))
                    {
                        var pos = smiles.IndexOf(" |&");
                        if (pos != -1)
                        {
                            smiles = smiles.Substring(0, pos).Replace(" ", "");
                        }
                    }
                    return smiles;
                }
                catch { }
            }
            return "";
        }
        public string Smiles()
        {
            if (_io != null)
            {
                try
                {//无法计算具有重复单位的结构的质量
                    var smiles = _io.smiles();
                    if (!string.IsNullOrEmpty(smiles))
                    {
                        var pos = smiles.IndexOf(" |&");
                        if (pos != -1)
                        {
                            smiles = smiles.Substring(0, pos).Replace(" ", "");
                        }
                    }
                    return smiles;
                }
                catch { }
            }
            return "";
        }
        public string FormulaWithWhiteSpace()
        {
            if (_io != null)
            {
                var formula = _io.grossFormula();
                return formula;
            }
            return "";
        }
        public string Formula()
        {
            try
            {
                if (_io != null)
                {
                    var formula = _io.grossFormula();
                    if (!string.IsNullOrEmpty(formula))
                    {
                        formula = formula.Replace(" ", "");
                    }
                    return formula;
                }
            }
            catch { }
            return "";
        }
        public string FormulaHtml()
        {
            if (_io != null)
            {
                try
                {
                    var formula = _io.grossFormula();
                    if (!string.IsNullOrEmpty(formula))
                    {
                        string[] fa = formula.Split(' ');
                        string returnstr = "";
                        for (var i = 0; i < fa.Length; i++)
                        {
                            string str = fa[i];
                            string rstr = "";
                            bool foundnum = false;
                            for (var j = 0; j < str.Length; j++)
                            {
                                string c = str.Substring(j, 1);
                                System.Text.ASCIIEncoding asciiEncoding = new System.Text.ASCIIEncoding();
                                int intC = (int)asciiEncoding.GetBytes(c)[0];
                                if ((intC >= 48 && intC <= 57))
                                {
                                    foundnum = true;
                                    //数字的
                                    if (j == 0)
                                    {
                                        //理论上不会到这一步
                                        rstr = "<sub>" + str.Substring(0) + "</sub>";
                                    }
                                    else
                                    {
                                        rstr = str.Substring(0, j) + "<sub>" + str.Substring(j) + "</sub>";
                                    }
                                    break;
                                }

                            }
                            if (!foundnum)
                            {
                                rstr += str;
                            }
                            returnstr += rstr;
                        }
                        return returnstr;
                    }
                }
                catch { }
                return "";
            }
            return "";
        }

        public string MolWeight()
        {
            if (_io != null)
            {
                try
                {//无法计算具有重复单位的结构的质量
                    return _io.molecularWeight().ToString();
                }
                catch { }
            }
            return "";
        }
        public string MonoisotopicMass()
        {
            if (_io != null)
            {
                try
                {
                    return _io.monoisotopicMass().ToString();
                }
                catch { }
            }
            return "";
        }
        public string MostAbundantMass()
        {
            if (_io != null)
            {
                try
                {
                    return _io.mostAbundantMass().ToString();
                }
                catch { }
            }
            return "";
        }
        public string InChI()
        {

            IndigoInchi indigo_inchi = new IndigoInchi(ido);
            if (_io != null)
            {
                try
                {
                    return indigo_inchi.getInchi(_io);
                }
                catch { }
            }
            return "";
        }
        public string InChIKey()
        {
            IndigoInchi indigo_inchi = new IndigoInchi(ido);
            if (_io != null)
            {
                try
                {
                    string inchi = indigo_inchi.getInchi(_io); ;
                    if (!string.IsNullOrEmpty(inchi))
                    {
                        return indigo_inchi.getInchiKey(inchi);
                    }
                }
                catch { }
            }
            return "";
        }
        public string ToString(string mimetype = "")
        {
            if (_io != null)
            {
                switch (mimetype)
                {
                    case "smiles":
                        return Smiles();

                    case "inchi":
                        return InChI();
                    default:
                        return _io.molfile();

                }
            }
            return "";
        }
        public bool IsEmpty
        {
            get
            {
                return _io == null;
            }
        }
        public static string FormatSMILES(string smiles)
        {
            if (!string.IsNullOrEmpty(smiles))
            {
                if (smiles.IndexOf("|&") != -1)
                {
                    return smiles.Substring(0, smiles.IndexOf("|&")).Trim();
                }
            }
            return smiles;
        }
        public static string FormatMol(string mol)
        {
            string[] lines = mol.Split('\n');
            string al = "";
            foreach (string a in lines)
            {
                RegexOptions options = RegexOptions.None;
                Regex regex = new Regex(@"[ ]{2,}", options);
                string tempo = "";
                tempo = a;
                string nstr = a;
                tempo = regex.Replace(tempo, @" ");
                string c = "#";
                if (tempo.Split(' ').Length == 8)
                {
                    nstr = nstr.Replace("0  0  1  0", "0  0  0  0");
                }
                string chars = "";
                foreach (char s in a)
                {
                    chars += Convert.ToInt32(s) + ",";
                }
                al += nstr + Environment.NewLine;
            }
            return al;
        }
        #endregion

        #region 生成结构式图片
        /// <summary>
        /// 生成结构式图片
        /// 常用参数详见：https://lifescience.opensource.epam.com/indigo/options/rendering-options.html
        /// </summary>
        /// <param name="w">宽度</param>
        /// <param name="h">高度</param>
        /// <param name="margin"></param>
        /// <param name="thickness">线条粗度</param>
        /// <returns></returns>
        public System.Drawing.Bitmap ToImage(int w = 500, int h = 500, int margin = 10, string thickness = "1.5", Dictionary<string, string> optipons = null, bool def = true)
        {
            if (margin <= 0) margin = 10;
            if (ido != null && _io != null)
            {
                if (optipons == null) optipons = new Dictionary<string, string>();
                IndigoRenderer ir = new IndigoRenderer(ido);
                if (optipons.Count > 0)
                {
                    foreach (var opt in optipons)
                    {
                        ido.setOption(opt.Key, opt.Value);
                    }
                    #region 默认参数
                    if (!optipons.ContainsKey("render-image-size"))
                    {
                        ido.setOption("render-image-size", w + "," + h);
                    }
                    if (!optipons.ContainsKey("render-margins"))
                    {
                        ido.setOption("render-margins", margin, margin);
                    }
                    if (!optipons.ContainsKey("render-relative-thickness"))
                    {
                        ido.setOption("render-relative-thickness", thickness);
                    }
                    #endregion
                }
                else
                {
                    ido.setOption("render-image-size", w + "," + h);
                    ido.setOption("render-margins", margin, margin);
                    ido.setOption("render-relative-thickness", thickness);
                }
                #region 默认参数
                if (def)
                {
                    if (!optipons.ContainsKey("molfile-saving-no-chiral"))
                    {
                        ido.setOption("molfile-saving-no-chiral", true);//不显示chiral 字样
                    }
                    if (!optipons.ContainsKey("render-stereo-style"))
                    {
                        ido.setOption("render-stereo-style", "none");
                    }
                    if (!optipons.ContainsKey("render-label-mode"))
                    {
                        ido.setOption("render-label-mode", "hetero");
                    }
                    if (!optipons.ContainsKey("render-superatom-mode"))
                    {
                        ido.setOption("render-superatom-mode", "collapse");
                    }
                    if (!optipons.ContainsKey("render-output-format"))
                    {
                        ido.setOption("render-output-format", "png");//图片格式
                    }
                    if (!optipons.ContainsKey("render-coloring"))
                    {
                        ido.setOption("render-coloring", true);//启用原子着色，例如氮为蓝色，氧为红色等
                    }
                    if (!optipons.ContainsKey("render-background-color"))
                    {
                        ido.setOption("render-background-color", "255,255,255");//背景颜色默认 白色
                    }
                    if (!optipons.ContainsKey("render-implicit-hydrogens-visible"))
                    {
                        ido.setOption("render-implicit-hydrogens-visible", true);//显示可见原子上的隐式氢
                    }
                    if (!optipons.ContainsKey("treat-x-as-pseudoatom"))
                    {
                        ido.setOption("treat-x-as-pseudoatom", "true");
                    }
                    if (!optipons.ContainsKey("render-highlight-thickness-enabled"))
                    {
                        ido.setOption("render-highlight-thickness-enabled", "true");
                    }

                    #region other 
                    //if (!optipons.ContainsKey("treat-x-as-pseudoatom"))
                    //{
                    //    ido.setOption("treat-x-as-pseudoatom", "true");
                    //}
                    //if (!optipons.ContainsKey("molfile-saving-no-chiral"))
                    //{
                    //    ido.setOption("molfile-saving-no-chiral", true);//不显示chiral 字样
                    //}
                    //if (!optipons.ContainsKey("ignore-stereochemistry-errors"))
                    //{
                    //    ido.setOption("ignore-stereochemistry-errors", true);
                    //}
                    //if (!optipons.ContainsKey("ignore-noncritical-query-features"))
                    //{
                    //    ido.setOption("ignore-noncritical-query-features", true);
                    //}
                    #endregion
                }
                #endregion
                System.Drawing.Bitmap bitmap = ir.renderToBitmap(_io);
                return bitmap;

            }
            return new System.Drawing.Bitmap(w, h);
        }

        #endregion

        #region 生成结构式图片
        /// <summary>
        /// 生成结构式图片
        /// 常用参数详见：https://lifescience.opensource.epam.com/indigo/options/rendering-options.html
        /// </summary>
        /// <param name="molorsmiles">mol或smiles</param>
        /// <param name="errmsg">返回错误信息</param>
        /// <param name="w">宽度 默认500</param>
        /// <param name="h">高度 默认500</param>
        /// <param name="margin"> 默认10</param>
        /// <param name="thickness">线条粗度 默认1.5</param>
        /// <param name="optipons">配置参数</param>
        /// <param name="def">是否默认参数 默认true</param>
        /// <param name="filename">生成图片名称，不含.png 默认当前时间</param>
        /// <param name="filepath">存放路径 默认当前根目录下images目录</param>
        /// <param name="watermarkpath">水印图片路径，无则说明无水印 默认无</param>
        /// <returns>生成图片的全路径，若为空则说明生成失败</returns>
        public string SaveImage(string molorsmiles, out string errmsg, int w = 500, int h = 500, int margin = 10, string thickness = "1.5", Dictionary<string, string> optipons = null, bool def = true, string filename = "structimg", string filepath = "", string watermarkpath = "")
        {
            errmsg = "";
            if (string.IsNullOrWhiteSpace(molorsmiles)) { errmsg = "Mol或Smiles文件为空，无法生成结构式图片！"; return null; }
            if (w <= 0) w = 500;
            if (h <= 0) h = 500;
            if (margin <= 0) margin = 10;
            if (string.IsNullOrWhiteSpace(thickness)) { thickness = "1.5"; }
            if (string.IsNullOrWhiteSpace(filepath)) { filepath = AppDomain.CurrentDomain.BaseDirectory + "images\\"; }
            if (string.IsNullOrWhiteSpace(filename)) { filename = DateTime.Now.ToString("yyyyMMddHHmmss"); }
            if (filename.Contains(".png")) { filename = filename.Replace(".png", ""); }
            if (optipons == null) optipons = new Dictionary<string, string>();

            using (Indigo ido = new Indigo())
            {
                try
                {
                    IndigoRenderer ir = new IndigoRenderer(ido);
                    if (optipons.Count > 0)
                    {
                        foreach (var opt in optipons)
                        {
                            ido.setOption(opt.Key, opt.Value);
                        }
                        #region 默认参数
                        if (!optipons.ContainsKey("render-image-size"))
                        {
                            ido.setOption("render-image-size", w + "," + h);
                        }
                        if (!optipons.ContainsKey("render-margins"))
                        {
                            ido.setOption("render-margins", margin, margin);
                        }
                        if (!optipons.ContainsKey("render-relative-thickness"))
                        {
                            ido.setOption("render-relative-thickness", thickness);
                        }
                        #endregion
                    }
                    else
                    {
                        ido.setOption("render-image-size", w + "," + h);
                        ido.setOption("render-margins", margin, margin);
                        ido.setOption("render-relative-thickness", thickness);
                    }

                    #region 默认参数
                    if (def)
                    {
                        if (!optipons.ContainsKey("molfile-saving-no-chiral"))
                        {
                            ido.setOption("molfile-saving-no-chiral", true);//不显示chiral 字样
                        }
                        if (!optipons.ContainsKey("render-stereo-style"))
                        {
                            ido.setOption("render-stereo-style", "none");
                        }
                        if (!optipons.ContainsKey("render-label-mode"))
                        {
                            ido.setOption("render-label-mode", "hetero");
                        }
                        if (!optipons.ContainsKey("render-output-format"))
                        {
                            ido.setOption("render-output-format", "png");//图片格式
                        }
                        if (!optipons.ContainsKey("render-coloring"))
                        {
                            ido.setOption("render-coloring", true);//启用原子着色，例如氮为蓝色，氧为红色等
                        }
                        if (!optipons.ContainsKey("render-background-color"))
                        {
                            ido.setOption("render-background-color", "255,255,255");//背景颜色默认 白色
                        }
                        if (!optipons.ContainsKey("render-implicit-hydrogens-visible"))
                        {
                            ido.setOption("render-implicit-hydrogens-visible", true);//显示可见原子上的隐式氢
                        }
                        if (!optipons.ContainsKey("treat-x-as-pseudoatom"))
                        {
                            ido.setOption("treat-x-as-pseudoatom", "true");
                        }
                        if (!optipons.ContainsKey("render-highlight-thickness-enabled"))
                        {
                            ido.setOption("render-highlight-thickness-enabled", "true");
                        }

                        #region other 
                        //if (!optipons.ContainsKey("treat-x-as-pseudoatom"))
                        //{
                        //    ido.setOption("treat-x-as-pseudoatom", "true");
                        //}
                        //if (!optipons.ContainsKey("molfile-saving-no-chiral"))
                        //{
                        //    ido.setOption("molfile-saving-no-chiral", true);//不显示chiral 字样
                        //}
                        //if (!optipons.ContainsKey("ignore-stereochemistry-errors"))
                        //{
                        //    ido.setOption("ignore-stereochemistry-errors", true);
                        //}
                        //if (!optipons.ContainsKey("ignore-noncritical-query-features"))
                        //{
                        //    ido.setOption("ignore-noncritical-query-features", true);
                        //}
                        #endregion
                    }
                    #endregion

                    // 加载分子
                    IndigoObject molecule = ido.loadMolecule(molorsmiles);
                    if (molecule != null)
                    {
                        using (var bitmap = ir.renderToBitmap(molecule))
                        {
                            if (bitmap != null)
                            {
                                if (!System.IO.Directory.Exists(filepath))
                                {
                                    System.IO.Directory.CreateDirectory(filepath);
                                }

                                var filefullpath = $"{filepath}{filename}.png";
                                if (!string.IsNullOrWhiteSpace(watermarkpath))
                                {
                                    // 加载水印图
                                    using (Bitmap watermarkImage = (Bitmap)Image.FromFile(watermarkpath))
                                    {
                                        // 创建一个新图层，大小与原图相同
                                        using (Bitmap bitmap_wm = new Bitmap(bitmap.Width, bitmap.Height))
                                        {
                                            using (Graphics graphics = Graphics.FromImage(bitmap_wm))
                                            {
                                                // 在新图层上绘制原图
                                                graphics.DrawImage(bitmap, 0, 0);
                                                // 设置水印位置（右上角）
                                                var position = new Point(10, 10);
                                                if (w <= 150)
                                                {
                                                    position = new Point(5, 5);
                                                }
                                                else if (w <= 300)
                                                {
                                                    position = new Point(8, 8);
                                                }
                                                // 绘制水印图像
                                                graphics.DrawImage(watermarkImage, position);
                                                // 保存新图像
                                                bitmap_wm.Save(filefullpath);
                                                return filefullpath;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    bitmap.Save(filefullpath);
                                    return filefullpath;
                                }

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    errmsg = $"生成结构式图片异常：{ex.Message} >> {ex.StackTrace}";
                }
            }
            return null;
        }
        #endregion

        #region 生成结构式图片
        private static Indigo indigo = null;
        private static IndigoRenderer indigorenderer = null;
        /// <summary>
        /// 结构式构造函数
        /// 常用参数详见：https://lifescience.opensource.epam.com/indigo/options/rendering-options.html
        /// </summary>
        /// <param name="w">宽度 默认500</param>
        /// <param name="h">高度 默认500</param>
        /// <param name="margin"> 默认10</param>
        /// <param name="thickness">线条粗度 默认1.5</param>
        /// <param name="optipons">配置参数</param>
        /// <param name="def">是否默认参数 默认true</param>
        public ChemMolecule(int w = 500, int h = 500, int margin = 10, string thickness = "1.5", Dictionary<string, string> optipons = null, bool def = true)
        {
            if (w <= 0) w = 500;
            if (h <= 0) h = 500;
            if (margin <= 0) margin = 10;
            if (string.IsNullOrWhiteSpace(thickness)) { thickness = "1.5"; }
            if (optipons == null) optipons = new Dictionary<string, string>();

            indigo = new Indigo();
            indigorenderer = new IndigoRenderer(indigo);
            if (optipons.Count > 0)
            {
                foreach (var opt in optipons)
                {
                    indigo.setOption(opt.Key, opt.Value);
                }
                #region 默认参数
                if (!optipons.ContainsKey("render-image-size"))
                {
                    indigo.setOption("render-image-size", w + "," + h);
                }
                if (!optipons.ContainsKey("render-margins"))
                {
                    indigo.setOption("render-margins", margin, margin);
                }
                if (!optipons.ContainsKey("render-relative-thickness"))
                {
                    indigo.setOption("render-relative-thickness", thickness);
                }
                #endregion
            }
            else
            {
                indigo.setOption("render-image-size", w + "," + h);
                indigo.setOption("render-margins", margin, margin);
                indigo.setOption("render-relative-thickness", thickness);
            }

            #region 默认参数
            if (def)
            {
                if (!optipons.ContainsKey("molfile-saving-no-chiral"))
                {
                    indigo.setOption("molfile-saving-no-chiral", true);//不显示chiral 字样
                }
                if (!optipons.ContainsKey("render-stereo-style"))
                {
                    indigo.setOption("render-stereo-style", "none");
                }
                if (!optipons.ContainsKey("render-label-mode"))
                {
                    indigo.setOption("render-label-mode", "hetero");
                }
                if (!optipons.ContainsKey("render-output-format"))
                {
                    indigo.setOption("render-output-format", "png");//图片格式
                }
                if (!optipons.ContainsKey("render-coloring"))
                {
                    indigo.setOption("render-coloring", true);//启用原子着色，例如氮为蓝色，氧为红色等
                }
                if (!optipons.ContainsKey("render-background-color"))
                {
                    indigo.setOption("render-background-color", "255,255,255");//背景颜色默认 白色
                }
                if (!optipons.ContainsKey("render-implicit-hydrogens-visible"))
                {
                    indigo.setOption("render-implicit-hydrogens-visible", true);//显示可见原子上的隐式氢
                }
                if (!optipons.ContainsKey("treat-x-as-pseudoatom"))
                {
                    indigo.setOption("treat-x-as-pseudoatom", "true");
                }
                if (!optipons.ContainsKey("render-highlight-thickness-enabled"))
                {
                    indigo.setOption("render-highlight-thickness-enabled", "true");
                }

                #region other 
                //if (!optipons.ContainsKey("treat-x-as-pseudoatom"))
                //{
                //    ido.setOption("treat-x-as-pseudoatom", "true");
                //}
                //if (!optipons.ContainsKey("molfile-saving-no-chiral"))
                //{
                //    ido.setOption("molfile-saving-no-chiral", true);//不显示chiral 字样
                //}
                //if (!optipons.ContainsKey("ignore-stereochemistry-errors"))
                //{
                //    ido.setOption("ignore-stereochemistry-errors", true);
                //}
                //if (!optipons.ContainsKey("ignore-noncritical-query-features"))
                //{
                //    ido.setOption("ignore-noncritical-query-features", true);
                //}
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// 生成结构式图片
        /// </summary>
        /// <param name="molorsmiles">mol或smiles</param>
        /// <param name="errmsg">返回错误信息</param>
        /// <param name="w">宽度 默认500</param>
        /// <param name="filename">生成图片名称，不含.png 默认当前时间</param>
        /// <param name="filepath">存放路径 默认当前根目录下images目录</param>
        /// <param name="watermarkpath">水印图片路径，无则说明无水印 默认无</param>
        /// <returns>生成图片的全路径，若为空则说明生成失败</returns>
        public string SaveToImage(string molorsmiles, out string errmsg, int w = 500, string filename = "structimg", string filepath = "", string watermarkpath = "")
        {
            errmsg = "";
            if (string.IsNullOrWhiteSpace(molorsmiles)) { errmsg = "Mol或Smiles文件为空，无法生成结构式图片！"; return null; }
            if (string.IsNullOrWhiteSpace(filepath)) { filepath = AppDomain.CurrentDomain.BaseDirectory + "images\\"; }
            if (string.IsNullOrWhiteSpace(filename)) { filename = DateTime.Now.ToString("yyyyMMddHHmmss"); }
            if (filename.Contains(".png")) { filename = filename.Replace(".png", ""); }
            if (indigo == null || indigorenderer == null) { errmsg = "indigo对象未实例化！"; return null; }
            try
            {
                // 加载分子
                IndigoObject molecule = indigo.loadMolecule(molorsmiles);
                if (molecule != null)
                {
                    using (var bitmap = indigorenderer.renderToBitmap(molecule))
                    {
                        if (bitmap != null)
                        {
                            if (!System.IO.Directory.Exists(filepath))
                            {
                                System.IO.Directory.CreateDirectory(filepath);
                            }

                            var filefullpath = $"{filepath}{filename}.png";
                            if (!string.IsNullOrWhiteSpace(watermarkpath))
                            {
                                // 加载水印图
                                using (Bitmap watermarkImage = (Bitmap)Image.FromFile(watermarkpath))
                                {
                                    // 创建一个新图层，大小与原图相同
                                    using (Bitmap bitmap_wm = new Bitmap(bitmap.Width, bitmap.Height))
                                    {
                                        using (Graphics graphics = Graphics.FromImage(bitmap_wm))
                                        {
                                            // 在新图层上绘制原图
                                            graphics.DrawImage(bitmap, 0, 0);
                                            // 设置水印位置（右上角）
                                            var position = new Point(10, 10);
                                            if (w <= 150)
                                            {
                                                position = new Point(5, 5);
                                            }
                                            else if (w <= 300)
                                            {
                                                position = new Point(8, 8);
                                            }
                                            // 绘制水印图像
                                            graphics.DrawImage(watermarkImage, position);
                                            // 保存新图像
                                            bitmap_wm.Save(filefullpath);
                                            return filefullpath;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                bitmap.Save(filefullpath);
                                return filefullpath;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errmsg = $"生成结构式图片异常：{ex.Message} >> {ex.StackTrace}";
            }
            return null;
        }

        #endregion


        #region 通过MOL/SMILES获取结构式信息
        /// <summary>
        /// 通过MOL/SMILES获取结构式信息
        /// </summary>
        /// <param name="molorsmiles">支持MOL，SMILES</param>
        /// <returns></returns>
        public static ChemMolecule GetMolecule(string molorsmiles)
        {
            return GetMolecule(molorsmiles, null);
        }
        /// <summary>
        /// 通过MOL/SMILES获取结构式信息
        /// </summary>
        /// <param name="molorsmiles">支持MOL，SMILES</param>
        /// <param name="optipons">自定义参数 详情见：https://lifescience.opensource.epam.com/indigo/options/rendering-options.html </param>
        /// <returns></returns>
        public static ChemMolecule GetMolecule(string molorsmiles, Dictionary<string, string> optipons)
        {
            string errmsg = string.Empty;
            return GetMolecule(molorsmiles, optipons, ref errmsg);
        }
        /// <summary>
        /// 通过MOL/SMILES获取结构式信息
        /// </summary>
        /// <param name="molorsmiles">支持MOL，SMILES</param>
        /// <param name="optipons">自定义参数 详情见：https://lifescience.opensource.epam.com/indigo/options/rendering-options.html </param>
        /// <param name="errmsg">错误消息</param>
        /// <returns></returns>
        public static ChemMolecule GetMolecule(string molorsmiles, Dictionary<string, string> optipons, ref string errmsg)
        {
            Indigo ido = new Indigo();
            try
            {
                IndigoRenderer ir = new IndigoRenderer(ido);
                molorsmiles = FormatSMILES(molorsmiles);
                if (optipons != null && optipons.Count > 0)
                {
                    foreach (var opt in optipons)
                    {
                        ido.setOption(opt.Key, opt.Value);
                    }
                }
                IndigoObject io = ido.loadMolecule(molorsmiles);
                ChemMolecule mol = null;
                if (io != null)
                {
                    mol = new ChemMolecule(ido, io);
                }
                return mol;
            }
            catch (Exception ex)
            {
                errmsg = "异常：" + ex.Message;
                return null;
            }
        }
        #endregion

        /// <summary>
        /// 打开帮助文档
        /// https://lifescience.opensource.epam.com/indigo/options/rendering-options.html
        /// </summary>
        public static void Help()
        {
            System.Diagnostics.Process.Start("https://lifescience.opensource.epam.com/indigo/options/rendering-options.html");
        }
    }
}
