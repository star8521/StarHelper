﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace StarOSWatch
{
    public class OsDrives
    {
        public static List<OsDrive> GetDrivesInfo()
        {
            List<OsDrive> drives = new List<OsDrive>();
            foreach (var d in DriveInfo.GetDrives())
            {
                if (d.DriveType == DriveType.CDRom)
                {
                    continue;
                }
                if (d.TotalSize >= 1024 * 1024)//磁盘大于1M
                {
                    double freeSpace = 0;
                    if (d.TotalFreeSpace > 0)
                    {
                        freeSpace = Math.Round(d.TotalFreeSpace / 1024 / 1024 / 1024.000, 3);
                    }
                    var totalSpace = Math.Round(d.TotalSize / 1024 / 1024 / 1024.000, 3);
                    drives.Add(new OsDrive() { Name = d.Name.Replace(":\\", ":"), Total = totalSpace, Free = freeSpace });
                }
            }
            return drives;
        }
    }

    public class OsDrive
    {
        public string Name { get; set; }
        public double Total { get; set; }
        public double Free { get; set; }
    }
}
