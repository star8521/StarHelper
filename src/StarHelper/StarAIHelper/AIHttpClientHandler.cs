﻿using Microsoft.SemanticKernel;
using Microsoft.SemanticKernel.ChatCompletion;
using Microsoft.SemanticKernel.Connectors.OpenAI;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace StarAIHelper
{
    public class AiHttpClientHandler : HttpClientHandler
    {
        private readonly string _uri;

        public AiHttpClientHandler(string uri) => _uri = uri.TrimEnd('/');

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            UriBuilder uriBuilder;
            if (request.RequestUri?.LocalPath == "/v1/chat/completions")
            {
                uriBuilder = new UriBuilder(_uri + "/v1/chat/completions");
                request.RequestUri = uriBuilder.Uri;
            }
            else if (request.RequestUri?.LocalPath == "/v1/embeddings")
            {
                uriBuilder = new UriBuilder(_uri + "/v1/embeddings");
                request.RequestUri = uriBuilder.Uri;
            }

            return await base.SendAsync(request, cancellationToken);
        }
    }



    public class HttpClientFunction(IHttpClientFactory httpClientFactory, IChatCompletionService completionService)
    {
        private const string BingTemplate = "https://cn.bing.com/search?q={0}";

        private const string SystemTemplate =
            @"
## 角色：

你是一款专业的搜索引擎助手。你的主要任务是从Html根据标签生成md的内容，并专注于准确地总结段落的大意，而不包含任何其他多余的信息或解释。

## 能力：

- 解析html中标签生成对应的md。
- 将提取的信息准确地总结为一段简洁的文本。
- 不属于用户提问的数据则不用整理。

## 指南：

- 这是一个完整的html标签，您需要根据标签生成对应的md格式。
- 只包含关键信息，尽量减少非主要信息的出现。
- 完成总结后，立即向用户提供，不需要询问用户是否满意或是否需要进一步的修改和优化。
";

        /// <summary>
        /// 搜索用户提出的问题
        /// </summary>
        [KernelFunction, Description("搜索用户提出的问题")]
        public async Task<string> GetAsync(string value)
        {
            var http = httpClientFactory.CreateClient(nameof(HttpClientFunction));
            var html = await http.GetStringAsync(string.Format(BingTemplate, value)).ConfigureAwait(false);

            var scriptRegex = new Regex(@"<script[^>]*>[\s\S]*?</script>");
            var styleRegex = new Regex(@"<style[^>]*>[\s\S]*?</style>");
            var commentRegex = new Regex(@"<!--[\s\S]*?-->");
            var headRegex = new Regex(@"<head[^>]*>[\s\S]*?</head>");
            var tagAttributesRegex = new Regex(@"<(\w+)(?:\s+[^>]*)?>");
            var emptyTagsRegex = new Regex(@"<(\w+)(?:\s+[^>]*)?>\s*</\1>");

            html = scriptRegex.Replace(html, "");
            html = styleRegex.Replace(html, "");
            html = commentRegex.Replace(html, "");
            html = headRegex.Replace(html, "");
            html = tagAttributesRegex.Replace(html, "<$1>");
            html = emptyTagsRegex.Replace(html, "");

            var result = await completionService.GetChatMessageContentsAsync(new ChatHistory(SystemTemplate){
                new(AuthorRole.User, html),
                new(AuthorRole.User, value)
            }, new OpenAIPromptExecutionSettings()
            {
                ModelId = "gpt-3.5-turbo-0125"
            });

            Console.WriteLine("搜索结果：" + result.FirstOrDefault()?.Content);

            return result.FirstOrDefault()?.Content ?? "抱歉，未找到相关信息。";
        }
    }
}
