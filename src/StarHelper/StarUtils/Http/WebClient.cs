﻿using System.Net.Http;

namespace StarUtils.Http
{
    public class WebClient
    {
        private readonly HttpClient _client;

        public WebClient(HttpClient client)
        {
            _client = client;
        }

        public IHttpRequest Get(string url)
        {
            return new HttpRequest(HttpMethod.Get, url, _client);
        }

        public IHttpRequest Post(string url)
        {
            return new HttpRequest(HttpMethod.Post, url, _client);
        }

        public IHttpRequest Put(string url)
        {
            return new HttpRequest(HttpMethod.Put, url, _client);
        }

        public IHttpRequest Delete(string url)
        {
            return new HttpRequest(HttpMethod.Delete, url, _client);
        }
    }
    public class WebClient<TResult> where TResult : class
    {
        private readonly HttpClient _client;

        public WebClient(HttpClient client)
        {
            _client = client;
        }

        public IHttpRequest<TResult> Get(string url)
        {
            return new HttpRequest<TResult>(HttpMethod.Get, url, _client);
        }

        public IHttpRequest<TResult> Post(string url)
        {
            return new HttpRequest<TResult>(HttpMethod.Post, url, _client);
        }

        public IHttpRequest<TResult> Put(string url)
        {
            return new HttpRequest<TResult>(HttpMethod.Put, url, _client);
        }

        public IHttpRequest<TResult> Delete(string url)
        {
            return new HttpRequest<TResult>(HttpMethod.Delete, url, _client);
        }
    }
}
