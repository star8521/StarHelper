﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StarUtils.Http
{
    public interface IRequest<out TRequest> where TRequest : IRequest<TRequest>
    {
        TRequest Encoding(Encoding encoding);

        TRequest Encoding(string encoding);

        TRequest ContentType(HttpContentType contentType);

        TRequest ContentType(string contentType);

        TRequest Cookie(string name, string value, double expiresDate);

        TRequest Cookie(string name, string value, DateTime expiresDate);

        TRequest Cookie(string name, string value, string path = "/", string domain = null, DateTime? expiresDate = null);

        TRequest Cookie(Cookie cookie);

        TRequest BearerToken(string token);

        TRequest Timeout(int timeout);

        TRequest Header<T>(string key, T value);

        TRequest Data(IDictionary<string, object> parameters);

        TRequest Data(string key, object value);

        TRequest JsonData<T>(T value);

        TRequest XmlData(string value);

        TRequest FileData(string filePath);

        TRequest FileData(string name, string filePath);

        TRequest OnFail(Action<string> action);

        TRequest OnFail(Action<string, HttpStatusCode> action);

        TRequest IgnoreSsl();

        Task<HttpResponseMessage> ResponseAsync();

        Task<string> ResultAsync();

        Task<Stream> ResultStream();

        Task<byte[]> ResultBytes();
    }


    public enum HttpContentType
    {
        [Description("application/x-www-form-urlencoded")]
        FormUrlEncoded,
        [Description("application/json")]
        Json,
        [Description("multipart/form-data")]
        FormData,
        [Description("text/xml")]
        Xml,
        [Description("application/grpc")]
        Grpc
    }
}
