﻿using Microsoft.Extensions.Configuration;

namespace StarUtils.Extension
{
    public static class AppSettings
    {
        private static readonly IConfigurationRoot config = ConfigUtils.GetJsonConfig();

        public static string Appsettings(this string key)
        {
            string result = string.Empty;
            if (config[key] != null)
            {
                result = config[key];
            }

            return result;
        }
    }
}
