﻿using StarUtils.Http;
using System;
using System.IO;

namespace StarUtils.Extension
{
    public static class Common
    {
        public static string Line => Environment.NewLine;

        public static Type GetType<T>()
        {
            return GetType(typeof(T));
        }

        public static Type GetType(Type type)
        {
            return Nullable.GetUnderlyingType(type) ?? type;
        }

        public static string GetPhysicalPath(string relativePath)
        {
            if (string.IsNullOrWhiteSpace(relativePath))
            {
                return string.Empty;
            }

            if (string.IsNullOrWhiteSpace(WebHelper.WebRootPath))
            {
                return Path.GetFullPath(relativePath);
            }

            return WebHelper.RootPath + "\\" + relativePath.Replace("/", "\\").TrimStart('\\');
        }

        public static string GetWebRootPath(string relativePath)
        {
            if (string.IsNullOrWhiteSpace(relativePath))
            {
                return string.Empty;
            }

            if (string.IsNullOrWhiteSpace(WebHelper.WebRootPath))
            {
                return Path.GetFullPath(relativePath);
            }

            return WebHelper.WebRootPath + "\\" + relativePath.Replace("/", "\\").TrimStart('\\');
        }
    }
}
