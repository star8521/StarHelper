﻿using System;

namespace StarUtils.Extension
{
    public static class TimeHelper
    {
        public static long GetJSTimespan(DateTime dt)
        {
            return (long)(dt.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalMilliseconds;
        }

        public static DateTime GetDateTimeFromJS(long dt)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(dt).AddHours(8.0);
        }
    }
}
