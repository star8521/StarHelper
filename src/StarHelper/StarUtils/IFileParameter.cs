﻿using System;
using System.IO;

namespace StarUtils.Extension
{

    public interface IFileParameter : IDisposable
    {
        Stream GetFileStream();

        string GetFileName();

        string GetName();
    }
    public class PhysicalFileParameter : IFileParameter, IDisposable
    {
        private FileStream _stream;

        public string AbsolutePath { get; }

        public string Name { get; }

        public PhysicalFileParameter(string absolutePath)
            : this(absolutePath, "files")
        {
        }

        public PhysicalFileParameter(string absolutePath, string name)
        {
            if (!File.Exists(absolutePath))
            {
                throw new FileNotFoundException("文件未找到：" + absolutePath);
            }

            AbsolutePath = absolutePath;
            Name = name;
        }

        public void Dispose()
        {
            _stream?.Dispose();
        }

        public Stream GetFileStream()
        {
            return _stream ?? (_stream = new FileStream(AbsolutePath, FileMode.Open));
        }

        public string GetFileName()
        {
            return Path.GetFileName(AbsolutePath);
        }

        public string GetName()
        {
            return Name;
        }
    }
}
