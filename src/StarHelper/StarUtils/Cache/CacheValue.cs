﻿namespace StarUtils.Cache
{
    public class CacheValue<T>
    {
        public bool HasValue { get; set; }

        public T Value { get; set; }

        public bool IsNull => Value == null;

        public static CacheValue<T> Null { get; } = new CacheValue<T>(default(T), hasvalue: true);


        public static CacheValue<T> NoValue { get; } = new CacheValue<T>(default(T), hasvalue: false);


        public CacheValue(T value, bool hasvalue)
        {
            Value = value;
            HasValue = hasvalue;
        }

        public override string ToString()
        {
            T value = Value;
            return ((value != null) ? value.ToString() : null) ?? "<NULL>";
        }
    }
}
