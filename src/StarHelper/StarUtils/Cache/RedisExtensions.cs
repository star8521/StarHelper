﻿using StackExchange.Redis;
using System.Threading.Tasks;

namespace StarUtils.Cache
{
    internal static class RedisExtensions
    {
        private const string HmGetScript = (@"return redis.call('HMGET', KEYS[1], unpack(ARGV))");// 通过脚本 HMGET 命令获取 key 的值
                                                                                                  //Get 方法中调用此方法, memebers 为固定值 data, 也就是获取字段 data 的值
        internal static RedisValue[] HashMemberGet(this IDatabase cache, string key, params string[] members)
        {
            var result = cache.ScriptEvaluate(
                HmGetScript,
                new RedisKey[] { key },
                GetRedisMembers(members));
            return (RedisValue[])result;
        }
        internal static async Task<RedisValue[]> HashMemberGetAsync(
            this IDatabase cache,
            string key,
            params string[] members)
        {
            var result = await cache.ScriptEvaluateAsync(
                HmGetScript,
                new RedisKey[] { key },
                GetRedisMembers(members));
            // TODO: Error checking?
            return (RedisValue[])result;
        }
        private static RedisValue[] GetRedisMembers(params string[] members)
        {
            var redisMembers = new RedisValue[members.Length];
            for (int i = 0; i < members.Length; i++)
            {
                redisMembers[i] = (RedisValue)members[i];
            }
            return redisMembers;
        }
    }
}
