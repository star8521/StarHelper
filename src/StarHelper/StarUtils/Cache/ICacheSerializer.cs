﻿using StarUtils.Extension;

namespace StarUtils.Cache
{
    public interface ICacheSerializer
    {
        string Serialize<T>(T value);

        T Deserialize<T>(string bytes);
    }
    internal class TextJsonFormaterSerializer : ICacheSerializer
    {
        public T Deserialize<T>(string bytes)
        {
            return bytes.ToObject<T>();
        }

        public string Serialize<T>(T value)
        {
            return value.ToJson();
        }
    }
}
