﻿using System;
using System.Collections.Generic;


namespace StarUtils.Cache
{
    public class RedisOptions
    {
        public RedisDbOptions DbConfig { get; set; } = new RedisDbOptions();


        public int MaxRdSecond { get; set; } = 120;


        public int Order { get; set; }

        public bool EnableLogging { get; set; }

        public string PrefixName { get; set; }
    }
    public class RedisDbOptions : RedisOptionsBase
    {
        public int Database { get; set; }
    }
    public class RedisOptionsBase
    {
        public IList<ServerEndPoint> EndPoints { get; } = new List<ServerEndPoint>();


        public string Password { get; set; }

        public int ConnectionTimeout { get; set; } = 5000;


        public bool IsSsl { get; set; }

        public string SslHost { get; set; }

        public bool AllowAdmin { get; set; }

        public string Configuration { get; set; } = "";

    }
    public class ServerEndPoint
    {
        public string Host { get; set; }

        public int Port { get; set; }

        public ServerEndPoint()
        {
        }

        public ServerEndPoint(string host, int port)
        {
            if (string.IsNullOrWhiteSpace(host))
            {
                throw new ArgumentNullException("host");
            }

            Host = host;
            Port = port;
        }
    }
}
