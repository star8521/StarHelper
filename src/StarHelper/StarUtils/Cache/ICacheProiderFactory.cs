﻿using System;
using System.Collections.Generic;
using System.Linq;
using StarUtils.Extension;

namespace StarUtils.Cache
{
    public interface ICacheProiderFactory
    {
        ICacheProvider GetCacheProvider(string name);
    }
    public class DefaultCacheProviderFactory : ICacheProiderFactory
    {
        private readonly IEnumerable<ICacheProvider> _cacheProviders;

        public DefaultCacheProviderFactory(IEnumerable<ICacheProvider> cacheProviders)
        {
            _cacheProviders = cacheProviders;
        }

        public ICacheProvider GetCacheProvider(string name)
        {
            if (name.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException("name");
            }

            return _cacheProviders.FirstOrDefault((ICacheProvider x) => x.Name != null && x.Name.Equals(name)) ?? throw new ArgumentException("找不到匹配的缓存提供程序");
        }
    }
}
