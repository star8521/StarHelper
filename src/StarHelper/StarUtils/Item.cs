﻿using Newtonsoft.Json;
using System;

namespace StarUtils.Extension
{
    public class JsonItem : IComparable<JsonItem>
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public object Value { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? SortId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Group { get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? Disabled { get; }

        public JsonItem(string text, object value, int? sortId = null, string group = null, bool? disabled = null)
        {
            Text = text;
            Value = value;
            SortId = sortId;
            Group = group;
            Disabled = disabled;
        }

        public int CompareTo(JsonItem other)
        {
            return string.Compare(Text, other.Text, StringComparison.CurrentCulture);
        }
    }
}
