﻿using System.Runtime.InteropServices;

namespace StarUtils.Extension
{
    public static class Sys
    {
        public static bool IsLinux => RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

        public static bool IsWindows => RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        public static bool IsOsx => RuntimeInformation.IsOSPlatform(OSPlatform.OSX);

        public static string System
        {
            get
            {
                if (!IsWindows)
                {
                    if (!IsLinux)
                    {
                        if (!IsOsx)
                        {
                            return string.Empty;
                        }

                        return "OSX";
                    }

                    return "Linux";
                }

                return "Windows";
            }
        }
    }
}
