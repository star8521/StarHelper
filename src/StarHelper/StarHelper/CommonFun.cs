﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Text;

namespace StarHelper
{
    /// <summary>
    /// 工具类
    /// </summary>
    public static class CommonFun
    {
        public static string GUID => Guid.NewGuid().ToString("N");
        public static bool IsNull(this string s)
        {
            return string.IsNullOrWhiteSpace(s);
        }
        public static bool NotNull(this string s)
        {
            return !string.IsNullOrWhiteSpace(s);
        }
        public static int GetRandom(int minNum, int maxNum)
        {
            var seed = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
            return new Random(seed).Next(minNum, maxNum);
        }
        public static string GetSerialNumber(string prefix = "")
        {
            return prefix + DateTime.Now.ToString("yyyyMMddHHmmssfff") + GetRandom(1000, 9999).ToString();
        }
        public static object GetDefaultVal(string typename)
        {
            switch (typename)
            {
                case "Boolean": return false;
                case "DateTime": return default(DateTime);
                case "Date": return default(DateTime);
                case "Double": return 0.0;
                case "Single": return 0f;
                case "Decimal": return 0m;
                case "Int32": return 0;
                case "String": return string.Empty;
                default:
                    return null;
            }
        }
        public static void CoverNull<T>(T model) where T : class
        {
            if (model == null)
            {
                return;
            }
            var typeFromHandle = typeof(T);
            var properties = typeFromHandle.GetProperties();
            var array = properties;
            for (var i = 0; i < array.Length; i++)
            {
                var propertyInfo = array[i];
                if (propertyInfo.GetValue(model, null) == null)
                {
                    propertyInfo.SetValue(model, GetDefaultVal(propertyInfo.PropertyType.Name), null);
                }
            }
        }
        public static void CoverNull<T>(List<T> models) where T : class
        {
            if (models.Count == 0)
            {
                return;
            }
            foreach (var model in models)
            {
                CoverNull(model);
            }
        }
        public static bool ToBool(this object thisValue, bool errorvalue = false)
        {
            if (thisValue != null && thisValue != DBNull.Value && bool.TryParse(thisValue.ToString(), out bool reval))
            {
                return reval;
            }
            return errorvalue;
        }

        public static string ToJson(this object obj)
        {

            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }
        public static T ToObject<T>(this string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }

        #region 文件操作
        public static FileInfo[] GetFiles(string directoryPath)
        {
            if (!IsExistDirectory(directoryPath))
            {
                throw new DirectoryNotFoundException();
            }
            var root = new DirectoryInfo(directoryPath);
            return root.GetFiles();
        }
        public static bool IsExistDirectory(string directoryPath)
        {
            return Directory.Exists(directoryPath);
        }
        public static string ReadFile(string Path)
        {
            string s;
            if (!File.Exists(Path))
                s = "不存在相应的目录";
            else
            {
                var f2 = new StreamReader(Path, Encoding.Default);
                s = f2.ReadToEnd();
                f2.Close();
                f2.Dispose();
            }
            return s;
        }
        public static void FileMove(string OrignFile, string NewFile)
        {
            File.Move(OrignFile, NewFile);
        }
        public static void CreateDir(string dir)
        {
            if (dir.Length == 0) return;
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }
        #endregion
    }
}
