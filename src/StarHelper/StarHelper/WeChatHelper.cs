﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Text;
using System.Xml.Linq;
using System.Xml;

namespace StarHelper
{
    /// <summary>
    /// 微信小程序相关接口
    /// </summary>
    public class WeChatHelper
    {
        public static string SessionUrl(string appid, string secret, string code, string grant_type = "authorization_code")
        {
            return string.Format("https://api.weixin.qq.com/sns/jscode2session?appid={0}&secret={1}&js_code={2}&grant_type={3}", appid, secret, code, grant_type);
        }
        public static string AccessTokenUrl(string appid, string secret, string grant_type = "client_credential")
        {
            return string.Format("https://api.weixin.qq.com/cgi-bin/token?appid={0}&secret={1}&grant_type={2}", appid, secret, grant_type);
        }
        public static string PhoneUrl(string access_token)
        {
            return string.Format("https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token={0}", access_token);
        }
        public static WeChatViews.WeChatPhone DecryptedPhone(string encryptedDataStr, string session_key, string iv)
        {
            return CommonFun.ToObject<WeChatViews.WeChatPhone>(Decrypted(encryptedDataStr, session_key, iv));
        }
        public static WeChatViews.WeChatUser DecryptedUser(string encryptedDataStr, string session_key, string iv)
        {
            return CommonFun.ToObject<WeChatViews.WeChatUser>(Decrypted(encryptedDataStr, session_key, iv));
        }

        public static string GetWeChatPaySign(string orderno, decimal total, string openid, string Des = "微信支付", string ClientIp = "127.0.0.1", string FeeType = "CNY")
        {
            var url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
            var nstr = MakeNonceStr();
            var packageParameter = new Hashtable
        {
            { "appid", AppSettingsHelper.Get("WeChatPay:AppId") },
            { "body", Des },
            { "openid", openid },
            { "mch_id", AppSettingsHelper.Get("WeChatPay:MchId") },
            { "notify_url", AppSettingsHelper.Get("Domain") + "/payment/wxnotify" },
            { "nonce_str", nstr },
            { "out_trade_no", orderno },
            { "total_fee", ((int)(total * 100)).ToString() },
            { "spbill_create_ip", ClientIp },
            { "trade_type", "JSAPI" },
            { "fee_type", FeeType }
        };
            var sign = CreateMd5Sign(packageParameter);
            packageParameter.Add("sign", sign);
            var xe = PostDataToWeiXin(url, packageParameter);
            var timeStamp = MakeTimestamp();
            var prepayId = xe.Element("prepay_id").Value;
            nstr = xe.Element("nonce_str").Value;
            var paySignReqHandler = new Hashtable
        {
            { "appId", AppSettingsHelper.Get("WeChatPay:AppId") },
            { "nonceStr", nstr },
            { "signType", "MD5" },
            { "package", "prepay_id=" + prepayId },
            { "timeStamp", timeStamp.ToString() }
        };
            var paySign = CreateMd5Sign(paySignReqHandler);
            var obj = new
            {
                prepayid = prepayId,
                noncestr = nstr,
                timestamp = timeStamp,
                sign = paySign
            };
            return obj.ToJson();
        }

        public static (bool result, string msg) WeChatPayTrans(string openid, double total, string desc = "微信转账", string ClientIp = "127.0.0.1")
        {
            var url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
            var packageParameter = new Hashtable
        {
            { "mch_appid", AppSettingsHelper.Get("WeChatPay:AppId") },
            { "mchid", AppSettingsHelper.Get("WeChatPay:MchId") },
            { "nonce_str", MakeNonceStr() },
            { "openid", openid },
            { "check_name", "NO_CHECK" },
            { "partner_trade_no", CommonFun.GetSerialNumber("R") },
            { "amount", (total * 100).ToString() },
            { "desc", desc },
            { "spbill_create_ip", ClientIp }
        };

            var sign = CreateMd5Sign(packageParameter);
            packageParameter.Add("sign", sign);
            var xe = PostDataToWeiXin(url, packageParameter, true);
            var result_code = xe.Element("result_code").Value;
            if (result_code.Equals("SUCCESS"))
            {
                return (true, "微信转账成功");
            }
            else
            {
                return (false, "微信转账失败");
            }
        }

        public static (bool result, string msg) WeChatPayReturn(string out_trade_no, double total, double refund)
        {
            var url = "https://api.mch.weixin.qq.com/secapi/pay/refund";
            var packageParameter = new Hashtable
        {
            { "appid", AppSettingsHelper.Get("WeChatPay:AppId") },
            { "mch_id", AppSettingsHelper.Get("WeChatPay:MchId") },
            { "nonce_str", MakeNonceStr() },
            { "out_trade_no", out_trade_no },
            { "out_refund_no", CommonFun.GetSerialNumber("R") },
            { "total_fee", ((int)(total * 100)).ToString() },
            { "refund_fee", (refund * 100).ToString() }
        };
            var sign = CreateMd5Sign(packageParameter);
            packageParameter.Add("sign", sign);
            var xe = PostDataToWeiXin(url, packageParameter, true);
            var result_code = xe.Element("result_code").Value;
            if (result_code.Equals("SUCCESS"))
            {
                return (true, "微信退款成功");
            }
            else
            {
                return (false, "微信退款失败");
            }
        }

        #region 微信接口
        private static string Decrypted(string encryptedDataStr, string session_key, string iv)
        {
            var rijalg = Aes.Create();
            rijalg.KeySize = 128;
            rijalg.Padding = PaddingMode.PKCS7;
            rijalg.Mode = CipherMode.CBC;
            rijalg.Key = Convert.FromBase64String(session_key);
            rijalg.IV = Convert.FromBase64String(iv);
            var encryptedData = Convert.FromBase64String(encryptedDataStr);
            //解密 
            var decryptor = rijalg.CreateDecryptor(rijalg.Key, rijalg.IV);
            string result;
            using (var msDecrypt = new MemoryStream(encryptedData))
            {
                using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (var srDecrypt = new StreamReader(csDecrypt))
                    {
                        result = srDecrypt.ReadToEnd();
                    }
                }
            }
            return result;
        }

        private static string MakeNonceStr()
        {
            var timestap = DateTime.Now.ToString("yyyyMMddhhmmssffff");
            return GetMD5(timestap);
        }
        private static string GetMD5(string src)
        {
            var md5 = new MD5CryptoServiceProvider();
            var data = Encoding.UTF8.GetBytes(src);
            var md5data = md5.ComputeHash(data);
            md5.Clear();
            var retStr = BitConverter.ToString(md5data);
            retStr = retStr.Replace("-", "").ToUpper();
            return retStr;
        }
        private static string CreateMd5Sign(Hashtable parameters)
        {
            var sb = new StringBuilder();
            var akeys = new ArrayList(parameters.Keys);
            akeys.Sort();
            foreach (string k in akeys)
            {
                var v = (string)parameters[k];
                sb.Append(k + "=" + v + "&");
            }
            sb.Append("key=" + AppSettingsHelper.Get("WeChatPay:ApiKey"));
            var sign = GetMD5(sb.ToString());
            return sign;
        }
        private static string getXmlStr(Hashtable parameters)
        {
            var sb = new StringBuilder();
            sb.Append("<xml>");
            foreach (string k in parameters.Keys)
            {
                var v = (string)parameters[k];
                if (Regex.IsMatch(v, @"^[0-9.]$"))
                {
                    sb.Append("<" + k + ">" + v + "</" + k + ">");
                }
                else
                {
                    sb.Append("<" + k + "><![CDATA[" + v + "]]></" + k + ">");
                }
            }
            sb.Append("</xml>");
            return sb.ToString();
        }
        private static int MakeTimestamp()
        {
            var ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt32(ts.TotalSeconds);
        }
        private static XElement PostDataToWeiXin(string url, Hashtable parameters, bool cert = false)
        {
            var xmlStr = getXmlStr(parameters);
            var data = Encoding.UTF8.GetBytes(xmlStr);
            Stream responseStream;
            var request = WebRequest.Create(url) as HttpWebRequest;
            if (cert)
            {
                var cer = new X509Certificate2(AppSettingsHelper.Get("WeChatPay:CerPath"), AppSettingsHelper.Get("WeChatPay:CerPassword"), X509KeyStorageFlags.MachineKeySet);
                request.ClientCertificates.Add(cer);
            }
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            request.ContentLength = data.Length;
            var requestStream = request.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();
            try
            {
                responseStream = request.GetResponse().GetResponseStream();
            }
            catch (Exception e)
            {
                throw new Exception("响应失败：" + e.Message);
            }
            var str = string.Empty;
            using (var reader = new StreamReader(responseStream, Encoding.UTF8))
            {
                str = reader.ReadToEnd();
            }
            responseStream.Close();
            var xe = XElement.Parse(str);
            return xe;
        }
        public static bool OrderQuery(string transaction_id)
        {
            var url = "https://api.mch.weixin.qq.com/pay/orderquery";
            var packageParameter = new Hashtable
        {
            { "appid", AppSettingsHelper.Get("WeChatPay:AppId") },
            { "mch_id", AppSettingsHelper.Get("WeChatPay:MchId") },
            { "transaction_id", transaction_id },
            { "nonce_str", MakeNonceStr() }
        };
            var sign = CreateMd5Sign(packageParameter);
            packageParameter.Add("sign", sign);
            var xe = PostDataToWeiXin(url, packageParameter);
            var return_code = xe.Element("return_code").Value;
            if (return_code.Equals("SUCCESS"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }


    /// <summary>
    /// 微信相关
    /// </summary>
    public class WeChatViews
    {
        public class AccessTokenModel
        {
            public string access_token { get; set; }
            public int expires_in { get; set; }
            public string refresh_token { get; set; }
            public string openid { get; set; }
            public string scope { get; set; }
            public string unionid { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class SessionModel
        {
            public string session_key { get; set; }
            public string unionid { get; set; }
            public string openid { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class WeChatPhone
        {
            /// <summary>
            /// 手机号
            /// </summary>
            public string phoneNumber { get; set; }
            /// <summary>
            /// 无区号手机号
            /// </summary>
            public string purePhoneNumber { get; set; }
            /// <summary>
            /// 区号
            /// </summary>
            public string countryCode { get; set; }
        }
        public class WeChatUser
        {
            public string unionId { get; set; }
            public string openId { get; set; }
            public string nickName { get; set; }
            public int gender { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public string province { get; set; }
            public string avatarUrl { get; set; }
        }
        public class PhoneView
        {
            public string phoneNumber { get; set; }
            public string purePhoneNumber { get; set; }
            public string countryCode { get; set; }
        }
        public class WxPayData
        {
            //采用排序的Dictionary的好处是方便对数据包进行签名，不用再签名之前再做一次排序
            private SortedDictionary<string, object> m_values = new SortedDictionary<string, object>();

            /**
            * 设置某个字段的值
            * @param key 字段名
             * @param value 字段值
            */
            public void SetValue(string key, object value)
            {
                m_values[key] = value;
            }

            /**
            * 根据字段名获取某个字段的值
            * @param key 字段名
             * @return key对应的字段值
            */
            public object GetValue(string key)
            {
                m_values.TryGetValue(key, out object o);
                return o;
            }

            /**
             * 判断某个字段是否已设置
             * @param key 字段名
             * @return 若字段key已被设置，则返回true，否则返回false
             */
            public bool IsSet(string key)
            {
                m_values.TryGetValue(key, out object o);
                if (null != o)
                    return true;
                else
                    return false;
            }

            /**
            * @将Dictionary转成xml
            * @return 经转换得到的xml串
            * @throws WxPayException
            **/
            public string ToXml()
            {
                //数据为空时不能转化为xml格式
                if (0 == m_values.Count)
                {
                    //Log.Error(this.GetType().ToString(), "WxPayData数据为空!");
                    //throw new WxPayException("WxPayData数据为空!");
                }

                var xml = "<xml>";
                foreach (KeyValuePair<string, object> pair in m_values)
                {
                    //字段值不能为null，会影响后续流程
                    if (pair.Value == null)
                    {
                        //Log.Error(this.GetType().ToString(), "WxPayData内部含有值为null的字段!");
                        //throw new WxPayException("WxPayData内部含有值为null的字段!");
                    }

                    if (pair.Value.GetType() == typeof(int))
                    {
                        xml += "<" + pair.Key + ">" + pair.Value + "</" + pair.Key + ">";
                    }
                    else if (pair.Value.GetType() == typeof(string))
                    {
                        xml += "<" + pair.Key + ">" + "<![CDATA[" + pair.Value + "]]></" + pair.Key + ">";
                    }
                    else//除了string和int类型不能含有其他数据类型
                    {
                        //Log.Error(this.GetType().ToString(), "WxPayData字段数据类型错误!");
                        //throw new WxPayException("WxPayData字段数据类型错误!");
                    }
                }
                xml += "</xml>";
                return xml;
            }

            /**
            * @将xml转为WxPayData对象并返回对象内部的数据
            * @param string 待转换的xml串
            * @return 经转换得到的Dictionary
            * @throws WxPayException
            */
            public SortedDictionary<string, object> FromXml(string xml, string ApiKey)
            {
                if (string.IsNullOrEmpty(xml))
                {
                    //Log.Error(this.GetType().ToString(), "将空的xml串转换为WxPayData不合法!");
                    //throw new WxPayException("将空的xml串转换为WxPayData不合法!");
                }

                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);
                var xmlNode = xmlDoc.FirstChild;//获取到根节点<xml>
                var nodes = xmlNode.ChildNodes;
                foreach (var xn in nodes)
                {
                    var xe = (XmlElement)xn;
                    m_values[xe.Name] = xe.InnerText;//获取xml的键值对到WxPayData内部的数据中
                }

                try
                {
                    CheckSign(ApiKey);//验证签名,不通过会抛异常
                }
                catch (Exception)
                {
                    //throw new WxPayException(ex.Message);
                }

                return m_values;
            }

            /**
            * @Dictionary格式转化成url参数格式
            * @ return url格式串, 该串不包含sign字段值
            */
            public string ToUrl()
            {
                var buff = "";
                foreach (KeyValuePair<string, object> pair in m_values)
                {
                    if (pair.Value == null)
                    {
                        //Log.Error(this.GetType().ToString(), "WxPayData内部含有值为null的字段!");
                        //throw new WxPayException("WxPayData内部含有值为null的字段!");
                    }

                    if (pair.Key != "sign" && pair.Value.ToString() != "")
                    {
                        buff += pair.Key + "=" + pair.Value + "&";
                    }
                }
                buff = buff.Trim('&');
                return buff;
            }



            /**
            * @生成签名，详见签名生成算法
            * @return 签名, sign字段不参加签名
            */
            public string MakeSign(string ApiKey)
            {
                //转url格式
                var str = ToUrl();
                //在string后加入API KEY
                str += "&key=" + ApiKey;
                //MD5加密
                var md5 = MD5.Create();
                var bs = md5.ComputeHash(Encoding.UTF8.GetBytes(str));
                var sb = new StringBuilder();
                foreach (var b in bs)
                {
                    sb.Append(b.ToString("x2"));
                }
                //所有字符转为大写
                return sb.ToString().ToUpper();
            }

            /**
            * 
            * 检测签名是否正确
            * 正确返回true，错误抛异常
            */
            public bool CheckSign(string ApiKey)
            {
                //如果没有设置签名，则跳过检测
                if (!IsSet("sign"))
                {
                    return true;
                }
                //如果设置了签名但是签名为空，则抛异常
                else if (GetValue("sign") == null || GetValue("sign").ToString() == "")
                {
                    //Log.Error(this.GetType().ToString(), "WxPayData签名存在但不合法!");
                    //throw new WxPayException("WxPayData签名存在但不合法!");
                }

                //获取接收到的签名
                var return_sign = GetValue("sign").ToString();

                //在本地计算新的签名
                var cal_sign = MakeSign(ApiKey);

                if (cal_sign == return_sign)
                {
                    return true;
                }

                //Log.Error(this.GetType().ToString(), "WxPayData签名验证错误!");
                //throw new WxPayException("WxPayData签名验证错误!");
                return false;
            }

            /**
            * @获取Dictionary
            */
            public SortedDictionary<string, object> GetValues()
            {
                return m_values;
            }
        }
    }

}
