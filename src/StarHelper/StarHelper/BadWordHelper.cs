﻿using System.Collections.Generic;

namespace StarHelper
{
    public static class BadWordHelper
    {
        /// <summary>
        ///  敏感词过滤 可深度
        /// </summary>
        /// <param name="content">欲过滤的内容</param>
        /// <param name="badwords">敏感词库</param>
        /// <param name="badword_str">返回涉及到的敏感词</param>
        /// <param name="result_str">执行过滤之后的内容</param>
        /// <param name="filter_deep">检测深度，即s_filters数组中的每个词中的插入几个字以内会被过滤掉，例：检测深度为2，敏感词库中有个词是中国，那么“中国”、“中*国”，“中**国”都会被过滤掉（*是任意字）。</param>
        /// <param name="check_only">是否只检测而不执行过滤操作</param>
        /// <param name="bTrim">过滤之前是否要去掉头尾的空字符</param>
        /// <param name="replace_str">将检测到的敏感字替换成的字符</param>
        /// <returns>是否含敏感词</returns>
        public static bool BadWordFilter(string content, string[] badwords, out string result_str, int filter_deep = 1, bool check_only = false, bool bTrim = false, string replace_str = "*")
        {
            string result = content;

            if (string.IsNullOrWhiteSpace(content))
            {
                result_str = "";
                return false;
            }
            if (bTrim)
            {
                result = (result + "").Trim();
            }
            result_str = result;
            if (badwords == null || badwords.Length == 0)
            {
                return false;
            }

            bool check = false;
            foreach (string str in badwords)
            {

                string s = System.String.IsNullOrWhiteSpace(replace_str) ? str : str.Replace(replace_str, "");
                if (s.Length == 0)
                {
                    continue;
                }

                bool bFiltered = true;
                while (bFiltered)
                {
                    int result_index_start = -1;
                    int result_index_end = -1;
                    int idx = 0;
                    while (idx < s.Length)
                    {
                        string one_s = s.Substring(idx, 1);
                        if (one_s == replace_str)
                        {
                            continue;
                        }
                        if (result_index_end + 1 >= result.Length)
                        {
                            bFiltered = false;
                            break;
                        }
                        int new_index = result.IndexOf(one_s, result_index_end + 1, System.StringComparison.OrdinalIgnoreCase);
                        if (new_index == -1)
                        {
                            bFiltered = false;
                            break;
                        }
                        if (idx > 0 && new_index - result_index_end > filter_deep + 1)
                        {
                            bFiltered = false;
                            break;
                        }
                        result_index_end = new_index;

                        if (result_index_start == -1)
                        {
                            result_index_start = new_index;
                        }
                        idx++;
                    }

                    if (bFiltered)
                    {
                        if (check_only)
                        {
                            return true;
                        }
                        check = true;
                        string result_left = result.Substring(0, result_index_start);
                        for (int i = result_index_start; i <= result_index_end; i++)
                        {
                            result_left += replace_str;
                        }
                        string result_right = result.Substring(result_index_end + 1);
                        result = result_left + result_right;
                    }
                }
            }
            result_str = result;
            return check;
        }

        /// <summary>
        /// 敏感词过滤 简单
        /// </summary>
        /// <param name="content">欲过滤的内容</param>
        /// <param name="badwords">敏感词库</param>
        /// <param name="badword_str">返回涉及到的敏感词</param>
        /// <returns>是否含敏感词</returns>
        public static bool BadWordFilter(string content, string[] badwords, out string badword_str)
        {
            if (badwords == null && badwords.Length == 0) { badword_str = ""; return false; }
            bool result = false;
            List<string> bword = new List<string>();

            for (int i = 0; i < badwords.Length; i++)
            {
                if (content.Contains(badwords[i]))
                {
                    if (!result)
                    {
                        result = true;
                    }
                    bword.Add(badwords[i]);
                }
            }
            badword_str = string.Join("、", bword);
            return result;
        }
    }
}
