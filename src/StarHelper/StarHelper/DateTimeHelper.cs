﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StarHelper
{
    /// <summary>
    /// 时间戳转换
    /// </summary>
    public static class DateTimeHelper
    {
        private const long STANDARD_TIME_STAMP = 621355968000000000;

        /// <summary>
        /// 时间转换为时间戳
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static long ConvertToTimeStamp(this DateTime time)
        {
            return (time.ToUniversalTime().Ticks - STANDARD_TIME_STAMP) / 10000000;
        }

        /// <summary>
        ///  时间戳转换为时间
        /// </summary>
        /// <param name="timestamp"></param>
        /// <returns></returns>
        public static DateTime ConvertToDateTime(this long timestamp)
        {
            return new DateTime(timestamp * 10000000 + STANDARD_TIME_STAMP).ToLocalTime();
        }


        /// <summary>
        /// 根据日期获取星期几
        /// </summary>
        public static string GetWeekByDate(DateTime dt)
        {
            var day = new[] { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
            return day[Convert.ToInt32(dt.DayOfWeek.ToString("d"))];
        }

        /// <summary>
        /// 得到这个月的第几周
        /// </summary>
        /// <param name="daytime">年月日</param>
        /// <returns>传递过来的时间是第几周</returns>
        public static int GetWeekNumInMonth(DateTime daytime)
        {
            int dayInMonth = daytime.Day;
            //本月第一天
            DateTime firstDay = daytime.AddDays(1 - daytime.Day);
            //本月第一天是周几
            int weekday = (int)firstDay.DayOfWeek == 0 ? 7 : (int)firstDay.DayOfWeek;
            //本月第一周有几天
            int firstWeekEndDay = 7 - (weekday - 1);
            //当前日期和第一周之差
            int diffday = dayInMonth - firstWeekEndDay;
            diffday = diffday > 0 ? diffday : 1;
            //当前是第几周,如果整除7就减一天
            int weekNumInMonth = ((diffday % 7) == 0
                ? (diffday / 7 - 1)
                : (diffday / 7)) + 1 + (dayInMonth > firstWeekEndDay ? 1 : 0);
            return weekNumInMonth;
        }
        /// <summary>
        /// 获取耗时
        /// </summary>
        /// <param name="btime">开始时间</param>
        /// <param name="etime">结束时间 默认当前</param>
        /// <returns></returns>
        public static string GetTimeSpan(DateTime btime, DateTime? etime = null)
        {
            if (etime == null) etime = DateTime.Now;
            TimeSpan ts = etime.Value.Subtract(btime);
            var hs = $"耗时{(ts.Days == 0 ? "" : (ts.Days + "天 "))}{(ts.Hours == 0 ? "" : (ts.Hours + "小时 "))} {(ts.Minutes == 0 ? "" : (ts.Minutes + "分钟 "))} {(ts.Seconds == 0 ? "" : (ts.Seconds + "秒 "))} {ts.Milliseconds}毫秒 [{ts.TotalMilliseconds}ms]";
            return hs;
        }
    }
}
