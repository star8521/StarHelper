﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace YYFStructrueSearchSDK
{
    public class HttpHelper
    {
        public class HttpResult
        {
            public int result { get; set; }
            public JObject data { get; set; }
            public string message { get; set; }
        }
        public HttpResult Get(string Url)
        {
            string ret = string.Empty;
            JObject jo = new JObject();

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = "GET";
                req.Timeout = 2 * 60 * 1000; //2分钟

                req.ContentType = "application/json";

                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                if (res != null)
                {
                    Stream s = res.GetResponseStream();
                    StreamReader sr = new StreamReader(s, Encoding.UTF8);
                    ret = sr.ReadToEnd();
                    sr.Close();
                    s.Close();
                    try
                    {

                        jo["result"] = 1;
                        jo["data"] = JObject.Parse(ret);
                    }
                    catch (Exception ex)
                    {
                        jo["result"] = -10002;
                        jo["data"] = ret;
                        jo["message"] = ex.Message;
                    }
                }
                else
                {
                    jo["result"] = "-10001";
                    jo["message"] = "未得到响应";
                }
                res.Close();
            }
            catch (Exception ex)
            {
                ret = ex.Message;
                jo["result"] = "-10000";
                jo["message"] = ex.Message;
            }

            return jo.ToObject<HttpResult>();
        }
        public HttpResult Post(string Url, string jsondata, string token)
        {
            string ret = string.Empty;
            JObject jo = new JObject();

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = "POST";
                req.Timeout = 2 * 60 * 1000; //2分钟

                byte[] btData = Encoding.UTF8.GetBytes(jsondata);
                req.ContentLength = btData.Length;
                req.ContentType = "application/json";

                req.Headers.Add("token", token);


                Stream sreq = req.GetRequestStream();
                sreq.Write(btData, 0, btData.Length);
                sreq.Close();

                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                if (res != null)
                {
                    Stream s = res.GetResponseStream();
                    StreamReader sr = new StreamReader(s, Encoding.UTF8);
                    ret = sr.ReadToEnd();
                    sr.Close();
                    s.Close();
                    try
                    {

                        jo["result"] = 1;
                        jo["data"] = JObject.Parse(ret);
                    }
                    catch (Exception ex)
                    {
                        jo["result"] = -10002;
                        jo["data"] = ret;
                        jo["message"] = ex.Message;
                    }
                }
                else
                {
                    jo["result"] = "-10001";
                    jo["message"] = "未得到响应";
                }
                res.Close();
            }
            catch (Exception ex)
            {
                ret = ex.Message;
                jo["result"] = "-10000";
                jo["message"] = ex.Message;
            }

            return jo.ToObject<HttpResult>();
        }


        public static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            //直接确认，否则打不开    
            return true;
        }
    }
}
