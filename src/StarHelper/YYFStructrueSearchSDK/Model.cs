﻿using System.Collections.Generic;

namespace YYFStructrueSearchSDK.Model
{
    #region 配置
    public class ChemStructureSearchConfig
    {
        public string ApiUrl { get; set; }
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public string CachedAccessToken { get; set; }
    }
    public enum ChemStructureSearchTypeEnum
    {
        /// <summary>
        /// 子结构
        /// </summary>
        sub,
        /// <summary>
        /// 精确结构
        /// </summary>
        exact,
        /// <summary>
        /// 相似结构
        /// </summary>
        similarity,
        /// <summary>
        /// 自定义字段搜索
        /// </summary>
        custom,
        /// <summary>
        /// 非结构式搜索
        /// </summary>
        none
    }
    #endregion

    #region 查询
    public class ChemStructureSearchRequestDto
    {
        public ChemStructureSearchTypeEnum searchType { get; set; } = ChemStructureSearchTypeEnum.none;
        public string smile { get; set; }

        public decimal similarity { get; set; }

        public int page { get; set; } = 1;
        public int psize { get; set; } = 20;

        /// <summary>
        /// 模糊查询
        /// </summary>
        public string kwd { get; set; }
        /// <summary>
        /// 和ES的查询一样
        /// </summary>
        public dynamic query { get; set; }
        /// <summary>
        /// 自定义参数用法：JObject othermust = new JObject();othermust["inchikey.keyword"] = "QXFXGHCEKFLEGF-AYVLZSQQSA-N";
        /// </summary>
        public dynamic othermust { get; set; }
    }

    public class ChemStructureSearchResultDto
    {
        public List<ChemStructureSearchResultItem> Items { get; set; }

        public List<dynamic> OrignalItems { get; set; }

        public int Total { get; set; }

        public int Page { get; set; }
        public int PageCount { get; set; }
        public int PSize { get; set; }

        public bool Success { get; set; }
        public string Message { get; set; }
    }
    /// <summary>
    /// 查询结果
    /// </summary>
    public class ChemStructureSearchResultItem
    {
        public string pkid { get; set; }
        public string libraryid { get; set; }
        public string type { get; set; }
        public string cas { get; set; }
        public string smile { get; set; }
    }
    #endregion

    #region 保存
    public class ChemStructureSaveDto
    {
        public List<ChemStructureSaveItem> Items { get; set; }
    }
    /// <summary>
    /// 保存数据
    /// </summary>
    public class ChemStructureSaveItem
    {
        #region 必要
        /// <summary>
        /// key 主键
        /// </summary>
        public string pkid { get; set; }
        /// <summary>
        /// key smile文件
        /// </summary>
        public string smile { get; set; }
        #endregion


        #region 非必要
        /// <summary>
        /// CAS号
        /// </summary>
        public string cas { get; set; }
        /// <summary>
        /// 中文名称
        /// </summary>
        public string cnName { get; set; }
        /// <summary>
        /// 英文名称
        /// </summary>
        public string enName { get; set; }
        /// <summary>
        /// mdl号
        /// </summary>
        public string mdl { get; set; }
        #endregion


        /// <summary>
        /// 其他自定义字段 用法如：new {type=1,pubchemId="pubchemId"}
        /// </summary>
        public dynamic others { get; set; }
    }
    public class ChemStructureSaveResult
    {
        public bool success { get; set; }
        public string message { get; set; }
        public int code { get; set; }
        public List<dynamic> errors { get; set; }
    }
    #endregion

    #region 删除
    public class ChemStructureDeleteDto
    {
        public List<string> pkids { get; set; }

    }
    public class ChemStructureDeleteResult
    {
        public bool success { get; set; }
        public string message { get; set; }
        public int code { get; set; }
        public List<dynamic> errors { get; set; }
        public object result { get; set; }
    }
    #endregion
}
