基于MongoDB.Entities的MongoDB数据库辅助类，封装了常用方法：
获取列表数据：db.List("users",JObject.FromObject({page:1,psize:1000,filter:JSON.stringify({"_id":id}),fields:JSON.stringify(['_id'])})).Result
获取单条数据：db.Get("users","xxxxxxx",fields).Result
通过过滤条件获取一条数据：db.GetOne("users",JObject.FromObject({username:'yyfs'}),fields).Result
新增修改数据：db.Save('users',JObject.FromObject(data),ret.optuser).Result
更新单条数据：db.UpdateOne("users",JObject.FromObject({username:'yyfs'}),JObject.FromObject({status:0}),ret.optuser).Result;
更新数据：db.Update("users",xxxxxxxx“”,JObject.FromObject({status:0}),ret.optuser).Result;
根据条件批量更新数据：db.UpdateMany("users",JObject.FromObject({status:0}),JObject.FromObject({status:1}),ret.optuser).Result;
var data=[
    { $match: { "id":id }},
    { $unwind: "$cost" },
    {
        $group : {
            _id : "$_id",        //必须是_id
            cost: { $sum : "$cost"}    //cost 可以是任意值
        }
    }
];
聚合查询： db.aggregate('users',true,data).Result;
创建删除索引：db.CreateDbIndex("users","abc",[{"a":"aaa"},{"b":"bbb"}]) db.DropDbIndex("users","abc")