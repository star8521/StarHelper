﻿using MongoDB.Bson;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace StarMongoDbHelper
{
    public class BSONHelper
    {
        public JObject parseToJObject(BsonDocument doc)
        {
            if (doc == null)
            {
                return null;
            }
            else
            {

                JObject jo = new JObject();
                var jsonstr = doc.ToJson();
                var props = getobjectidkeys(jsonstr);
                foreach (var key in props)
                {
                    var nv = key.Replace("ObjectId(", "{\"$oid\":").Replace(")", "}");
                    jsonstr = jsonstr.Replace(key, nv);
                }

                var nprops = getNumberLongkeys(jsonstr);
                foreach (var key in nprops)
                {
                    var nv = key.Replace("NumberLong(\"", "").Replace("\")", "");
                    jsonstr = jsonstr.Replace(key, nv);
                }
                var dprops = getISODatekeys(jsonstr);
                foreach (var key in dprops)
                {
                    var nv = key.Replace("ISODate(\"", "").Replace("\")", "");
                    DateTime xdt = DateTime.Parse(nv);
                    nv = xdt.ToString("yyyy-MM-dd HH:mm:ss");
                    jsonstr = jsonstr.Replace(key, nv);
                }

                jo = JObject.Parse(jsonstr);
                return jo;
            }

        }
        private string[] getobjectidkeys(string v)
        {

            List<string> keys = new List<string>();
            var regex = new Regex(@"ObjectId\(""(\w+)""\)");
            MatchCollection matches = regex.Matches(v);

            foreach (Match match in matches)
            {
                if (!string.IsNullOrEmpty(match.Value))
                {
                    keys.Add(match.Value);

                }
            }
            return keys.ToArray();
        }

        private string[] getNumberLongkeys(string v)
        {
            List<string> keys = new List<string>();
            // var regex = new Regex(@"ObjectId\((.+)aa37");
            var regex = new Regex(@"NumberLong\(""(\w+)""\)");
            MatchCollection matches = regex.Matches(v);

            foreach (Match match in matches)
            {
                if (!string.IsNullOrEmpty(match.Value))
                {
                    keys.Add(match.Value);

                }
            }
            return keys.ToArray();

        }

        //ISODate(

        private string[] getISODatekeys(string v)
        {
            List<string> keys = new List<string>();
            // var regex = new Regex(@"ObjectId\((.+)aa37");
            var regex = new Regex(@"ISODate\(""([\w.:-]+)""\)");
            MatchCollection matches = regex.Matches(v);

            foreach (Match match in matches)
            {
                if (!string.IsNullOrEmpty(match.Value))
                {
                    keys.Add(match.Value);

                }
            }
            return keys.ToArray();

        }
        public JArray parseToJArray(IList<BsonDocument> list)
        {

            string axx = list.ToJson();
            var props = getobjectidkeys(axx);
            foreach (var key in props)
            {
                var nv = key.Replace("ObjectId(", "{\"$oid\":").Replace(")", "}");
                axx = axx.Replace(key, nv);
            }


            var nprops = getNumberLongkeys(axx);
            foreach (var key in nprops)
            {
                var nv = key.Replace("NumberLong(\"", "").Replace("\")", "");
                axx = axx.Replace(key, nv);
            }

            var dprops = getISODatekeys(axx);
            foreach (var key in dprops)
            {
                var nv = key.Replace("ISODate(\"", "").Replace("\")", "");
                DateTime xdt = DateTime.Parse(nv);
                nv = "\"" + xdt.ToString("yyyy-MM-dd HH:mm:ss") + "\"";
                axx = axx.Replace(key, nv);
            }

            JArray ja = JArray.Parse(axx);
            return ja;
        }
    }
}
