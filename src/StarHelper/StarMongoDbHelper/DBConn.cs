﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace StarMongoDbHelper
{
    public class DBConn
    {
        public string appid { get; set; }
        public string id { get; set; }
        public string conn { get; set; }
        public string dbname { get; set; }
        public string dbtype { get; set; }
        public string entityname { get; set; }
    }
    public class DBResult
    {
        public bool success { get; set; }
        public string message { get; set; }
        public JObject entity { get; set; }

        public Exception ex { get; set; }
    }
    public class IndexKeyDto
    {
        public string key { get; set; }
        public JToken value { get; set; }
    }


    public class OrderByDto
    {
        public string key { get; set; }
        public bool desc { get; set; }
    }
    public class QueryListInput
    {
        /// <summary>
        /// 第几页，第一页=1
        /// </summary>
        public int page { get; set; } = 1;
        /// <summary>
        /// 每页显示条数
        /// </summary>
        public int psize { get; set; } = 10;

        public long total { get; set; } = 0;
        public int totalPages { get; set; } = 0;
        public string filter { get; set; } = "{}";

        public string sort { get; set; }

        /// <summary>
        /// 支持数据或者对象
        /// </summary>
        public string fields { get; set; }

        //求和字段
        public object sumfields { get; set; }

        /// <summary>
        /// 是否获取全部数据
        /// </summary>
        public bool gettotalcount { get; set; } = false;

        /// <summary>
        /// 数组拆分
        /// </summary>
        public string unwindby { get; set; }
        /// <summary>
        /// 聚合查询方案id
        /// </summary>
        public string aggregateid { get; set; }

        public string bodyjson { get; set; }
    }
    public class QueryListOutput
    {
        public object rows { get; set; }
        public object sumdata { get; set; }
        public JObject lookups { get; set; }
        /// <summary>
        /// 格式化数据的js函数
        /// </summary>
        public string dataformatter { get; set; }
        public QueryListInput param { get; set; }

        public QueryListOutput()
        {

        }
        public QueryListOutput(QueryListInput _param)
        {
            param = _param;
        }
    }



    #region 聚合查询
    public enum QueryType
    {
        count,
        list,
        getone
    }
    public enum AggregateQueryType
    {
        list,
        getone
    }
    public class QueryAggregate
    {

        /// <summary>
        /// count,list,getone
        /// </summary>
        public AggregateQueryType querytype { get; set; }
        public List<JObject> pipelines { get; set; }

    }
    public class AggregateOutput
    {
        public object data { get; set; }
        public string dataformatter { get; set; }
    } 
    #endregion
}
