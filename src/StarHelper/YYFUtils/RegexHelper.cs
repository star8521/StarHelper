﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace YYFUtils
{
    /// <summary>
    /// 正则表达式匹配
    /// </summary>
    public static class RegexHelper
    {
        /// <summary>
        /// 从文本内容中正则提取img标签图片链接列表
        /// </summary>
        /// <param name="content">含img标签图片的文本内容</param>
        /// <returns></returns>
        public static List<string> RegexImgs(this string content)
        {
            // 正则表达式匹配URL                
            Regex regImg = new Regex(@"<img\b[^<>]*?\bsrc[\s\t\r\n]*=[\s\t\r\n]*[""']?[\s\t\r\n]*(?<imgUrl>[^\s\t\r\n""'<>]*)[^<>]*?/?[\s\t\r\n]*>", RegexOptions.IgnoreCase);
            MatchCollection matches = regImg.Matches(content);
            List<string> list = new List<string>();
            // 下载并保存图片
            foreach (Match match in matches)
            {
                list.Add(match.Groups["imgUrl"].Value);
            }
            return list;
        }
    }
}
