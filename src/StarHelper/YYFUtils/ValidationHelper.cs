﻿using System;
using System.Text.RegularExpressions;

namespace YYFUtils
{
    public static class ValidationHelper
    {
        public const string EmailRegex = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
        public const string JsReplaceRegex = @"<script[^>]*>([\s\S](?!<script))*?</script>";


        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }
        /// <summary>
        /// 邮件验证
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsEmail(this string value)
        {
            if (value.IsNullOrEmpty())
            {
                return false;
            }

            var regex = new Regex(EmailRegex);
            return regex.IsMatch(value);
        }

        /// <summary>
        /// 手机号码验证
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsPhone(this string value)
        {
            if (value.IsNullOrEmpty()) return false;
            return Regex.IsMatch(value, @"^1[3456789]\d{9}$");
        }
        /// <summary>
        /// 电话号码验证("XXX-XXXXXXX"、"XXXX-XXXXXXXX"、"XXX-XXXXXXX"、"XXX-XXXXXXXX"、"XXXXXXX"和"XXXXXXXX)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsTel(this string value)
        {
            if (value.IsNullOrEmpty()) return false;
            return Regex.IsMatch(value, @"^(\(\d{3,4}-)|\d{3.4}-)?\d{7,8}$");
        }
        /// <summary>
        /// 身份证号码验证
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsIDCard(this string value)
        {
            if (value.IsNullOrEmpty()) return false;
            return Regex.IsMatch(value, @"^\d{15}|\d{18}$");
        }
        /// <summary>
        /// QQ号码验证
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsQQ(this string value)
        {
            if (value.IsNullOrEmpty()) return false;
            return Regex.IsMatch(value, "^[1-9][0-9]{4,}");
        }

        /// <summary>
        /// 判断一个字符串是否为url
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsUrl(this string value)
        {
            if (value.IsNullOrEmpty()) return false;
            return Regex.IsMatch(value, @"^http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?$");
        }
        /// <summary>
        /// 验证cas号是否是正确的
        /// </summary>
        /// <param name="cass"></param>
        /// <returns></returns>
        public static bool IsCas(this string cass)
        {
            if (cass.IsNullOrEmpty()) return false;
            cass = cass.Replace("–", "-");
            Regex r = new Regex("^[0-9]{2,9}-[0-9]{2}-[0-9]$");
            Match m = r.Match(cass);
            bool rv = false;
            if (m.Success)
            {
                string teststr = cass.Substring(0, cass.LastIndexOf('-'));
                string lastchar = cass.Replace(teststr, "").Replace("-", "");
                teststr = teststr.Replace("-", "");

                int total = 0;
                for (int i = 0; i < teststr.Length; i++)
                {
                    total += int.Parse(teststr.Substring(i, 1)) * (teststr.Length - i);
                }
                int oldtotal = total;
                int mod = 0;
                mod = total % 10;
                if (lastchar == mod.ToString())
                {
                    rv = true;
                }
            }
            return rv;
        }

        /// <summary>
        /// 把字符转化成cas号
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToCasNo(this string value)
        {
            if (value.IsNullOrEmpty()) return "";
            if (value.Length <= 3) return "";
            int first = value.Length - 3;
            int second = value.Length - 1;
            return $"{value.Substring(0, first)}-{value.Substring(first, 2)}-{value.Substring(second)}";
        }

        /// <summary>
        /// 把Cas号转化成cas url形式
        /// </summary>
        /// <param name="casNo"></param>
        /// <returns></returns>
        public static string ToCasUrl(this string casNo)
        {
            if (casNo.IsNullOrEmpty()) return "";
            return casNo.Trim().Replace("-", String.Empty);
        }

        /// <summary>
        /// 把制定文本中的 <script></script>标签替换为空格
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ReplaceJavaScript(this string value)
        {
            return value.IsNullOrEmpty() ? value : Regex.Replace(value, JsReplaceRegex, string.Empty, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// 判断是否有中文
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool WordsIScn(this string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                Regex rx = new Regex("^[\u4e00-\u9fa5]$");
                if (rx.IsMatch(s[i].ToString()))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 判断字符串中是否含有英文字符
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsEnglishValue(string value)
        {
            return Regex.Matches(value, "[a-zA-Z]").Count > 0;
        }
        /// <summary>
        /// 判断字符串中是否含有中文字符
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool HasChineseValue(string value)
        {
            bool falg = false;
            if (!string.IsNullOrEmpty(value) && Regex.IsMatch(value, @"[\u4e00-\u9fa5]+"))
            {
                falg = true;
            }
            return falg;
        }
        public static string FormatFZS(this string s)
        {


            string optStr = "";
            if (!string.IsNullOrEmpty(s))
            {
                s = s.Trim();
                s = s.Replace(".", "<sup>.</sup>");
                Regex reg = new Regex(@"(\d+)");
                s = reg.Replace(s, @"<sub>$1</sub>");
                optStr = s;
            }

            return optStr;
        }
        public static string ToCurrencySymbol(this string s)
        {
            switch (s)
            {
                case "Zh":
                case "zh-cn":
                    return "￥";

                case "De":

                    return " € ";
                case "En":
                    return "$";
                case "Jp":
                    return "¥";
                case "Kr":
                    return "₩";

            }
            return "";
        }

        public static string NoSQL(this string s)
        {
            if (s.IsNullOrEmpty()) return "";
            s = s.Trim().Replace("!", String.Empty);
            s = s.Trim().Replace("&", String.Empty);
            s = s.Trim().Replace("(", String.Empty);
            s = s.Trim().Replace(")", String.Empty);
            s = s.Trim().Replace("[", String.Empty);
            s = s.Trim().Replace("]", String.Empty);
            s = s.Trim().Replace("|", String.Empty);
            s = s.Trim().Replace("~", String.Empty);
            s = s.Trim().Replace(",", String.Empty);
            return s;
        }

        public static string RepStrSearch(this string s)
        {
            if (s.IsNullOrEmpty()) return "";
            return s.Replace("%22", "");
        }

        /// <summary>
        /// 计算密码强度
        /// </summary>
        /// <param name="password">密码字符串</param>
        /// <returns></returns>
        public static bool PasswordStrength(string password)
        {
            //空字符串强度值为0
            if (string.IsNullOrEmpty(password)) return false;

            //字符统计
            int iNum = 0, iLtt = 0, iSym = 0;
            foreach (char c in password)
            {
                if (c >= '0' && c <= '9') iNum++;
                else if (c >= 'a' && c <= 'z') iLtt++;
                else if (c >= 'A' && c <= 'Z') iLtt++;
                else iSym++;
            }

            if (iLtt == 0 && iSym == 0) return false; //纯数字密码
            if (iNum == 0 && iLtt == 0) return false; //纯符号密码
            if (iNum == 0 && iSym == 0) return false; //纯字母密码

            if (password.Length >= 6 && password.Length < 16) return true;//长度不大于6的密码

            if (iLtt == 0) return true; //数字和符号构成的密码
            if (iSym == 0) return true; //数字和字母构成的密码
            if (iNum == 0) return true; //字母和符号构成的密码

            return true; //由数字、字母、符号构成的密码
        }
    }
}
