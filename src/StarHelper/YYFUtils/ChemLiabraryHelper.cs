﻿using System.Text.RegularExpressions;

namespace YYFUtils
{

    public class ChemLiabraryHelper
    {
        private static int[,] CHEMIDENCODEVAR = new int[,]
        {
            {
                112,
                103,
                100,
                97,
                113,
                107,
                99,
                105
            },
            {
                97,
                113,
                98,
                112,
                99,
                106,
                111,
                109
            },
            {
                100,
                102,
                99,
                110,
                104,
                101,
                113,
                105
            },
            {
                110,
                97,
                113,
                98,
                107,
                100,
                111,
                105
            },
            {
                101,
                98,
                100,
                97,
                112,
                109,
                99,
                105
            },
            {
                99,
                112,
                98,
                109,
                100,
                105,
                109,
                97
            },
            {
                111,
                97,
                106,
                100,
                103,
                101,
                109,
                99
            },
            {
                107,
                99,
                102,
                108,
                98,
                105,
                100,
                104
            }
        };

        private static int CHEMIDENCODEMODVAR = 97;

        public static bool IsCas(object cas)
        {
            string text = cas as string;
            bool result = false;
            bool flag = !string.IsNullOrEmpty(text);
            if (flag)
            {
                Regex regex = new Regex("^[0-9]{2,7}-[0-9]{2}-[0-9]$");
                Match match = regex.Match(text);
                bool success = match.Success;
                if (success)
                {
                    string text2 = text.Substring(0, text.LastIndexOf('-'));
                    string a = text.Replace(text2, "").Replace("-", "");
                    text2 = text2.Replace("-", "");
                    int num = 0;
                    for (int i = 0; i < text2.Length; i++)
                    {
                        num += int.Parse(text2.Substring(i, 1)) * (text2.Length - i);
                    }
                    bool flag2 = a == (num % 10).ToString();
                    if (flag2)
                    {
                        result = true;
                    }
                }
            }
            return result;
        }



        public static string EncodeChemIdOfChar(long ChemId)
        {
            int num = (int)(ChemId % 8L);
            string text = string.Format("{0}", ChemId);
            return string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}", new object[]
            {
                (char)(int.Parse(text[7].ToString()) + ChemLiabraryHelper.CHEMIDENCODEVAR[num, 0]),
                (char)(int.Parse(text[5].ToString()) + ChemLiabraryHelper.CHEMIDENCODEVAR[num, 1]),
                (char)(int.Parse(text[6].ToString()) + ChemLiabraryHelper.CHEMIDENCODEVAR[num, 2]),
                (char)(int.Parse(text[3].ToString()) + ChemLiabraryHelper.CHEMIDENCODEVAR[num, 3]),
                (char)(int.Parse(text[4].ToString()) + ChemLiabraryHelper.CHEMIDENCODEVAR[num, 4]),
                (char)(int.Parse(text[1].ToString()) + ChemLiabraryHelper.CHEMIDENCODEVAR[num, 5]),
                (char)(int.Parse(text[2].ToString()) + ChemLiabraryHelper.CHEMIDENCODEVAR[num, 6]),
                (char)(int.Parse(text[0].ToString()) + ChemLiabraryHelper.CHEMIDENCODEVAR[num, 7]),
                (char)(num + ChemLiabraryHelper.CHEMIDENCODEMODVAR)
            });
        }

        public static long DecodeChemOfChar(string ChemId)
        {
            bool flag = string.IsNullOrEmpty(ChemId) || ChemId.Length != 9;
            long result;
            if (flag)
            {
                result = 0L;
            }
            else
            {
                string text = ChemId.ToString().ToLower();
                int num = (int)text[8] - ChemLiabraryHelper.CHEMIDENCODEMODVAR;
                bool flag2 = num > 7;
                if (flag2)
                {
                    result = 0L;
                }
                else
                {
                    string s = string.Format("{7}{5}{6}{3}{4}{1}{2}{0}", new object[]
                    {
                        (int)ChemId[0] - ChemLiabraryHelper.CHEMIDENCODEVAR[num, 0],
                        (int)ChemId[1] - ChemLiabraryHelper.CHEMIDENCODEVAR[num, 1],
                        (int)ChemId[2] - ChemLiabraryHelper.CHEMIDENCODEVAR[num, 2],
                        (int)ChemId[3] - ChemLiabraryHelper.CHEMIDENCODEVAR[num, 3],
                        (int)ChemId[4] - ChemLiabraryHelper.CHEMIDENCODEVAR[num, 4],
                        (int)ChemId[5] - ChemLiabraryHelper.CHEMIDENCODEVAR[num, 5],
                        (int)ChemId[6] - ChemLiabraryHelper.CHEMIDENCODEVAR[num, 6],
                        (int)ChemId[7] - ChemLiabraryHelper.CHEMIDENCODEVAR[num, 7]
                    });
                    long num2 = 0L;
                    bool flag3 = long.TryParse(s, out num2);
                    if (flag3)
                    {
                        result = num2;
                    }
                    else
                    {
                        result = 0L;
                    }
                }
            }
            return result;
        }

        public static string EncodeChemId(long ChemId)
        {
            return ChemLiabraryHelper.EncodeId(ChemId, "C");
        }

        public static long DecodeChemId(string ChemId)
        {
            return ChemLiabraryHelper.DecodeId(ChemId, "C");
        }

        public static string EncodeProductId(int ProductId)
        {
            return ChemLiabraryHelper.EncodeId((long)ProductId, "P");
        }

        public static int DecodeProductId(string ProductId)
        {
            return (int)ChemLiabraryHelper.DecodeId(ProductId, "P");
        }

        public static string EncodeShopId(int ShopId)
        {
            return ChemLiabraryHelper.EncodeId((long)ShopId, "S");
        }

        public static int DecodeShopIdId(string WordId)
        {
            return (int)ChemLiabraryHelper.DecodeId(WordId, "S");
        }

        public static string EncodeChanpinCiId(int WordId)
        {
            return ChemLiabraryHelper.EncodeId((long)WordId, "W");
        }

        public static int DecodeChanpinCiId(string ShopId)
        {
            return (int)ChemLiabraryHelper.DecodeId(ShopId, "W");
        }

        protected static string EncodeId(long Id, string Prefix)
        {
            string text = Id.ToString().PadLeft(8, '0');
            string text2 = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}", new object[]
            {
                9 - int.Parse(text[7].ToString()),
                9 - int.Parse(text[5].ToString()),
                9 - int.Parse(text[6].ToString()),
                9 - int.Parse(text[3].ToString()),
                9 - int.Parse(text[4].ToString()),
                9 - int.Parse(text[1].ToString()),
                9 - int.Parse(text[2].ToString()),
                9 - int.Parse(text[0].ToString())
            });
            int num = 0;
            for (int i = 1; i <= text2.Length; i++)
            {
                num += int.Parse(text2[i - 1].ToString()) * i;
            }
            int num2 = num % 9;
            return string.Format("{0}{1}{2}", Prefix, text2, num2);
        }

        protected static long DecodeId(string Id, string Prefix)
        {
            bool flag = string.IsNullOrEmpty(Id) || Id.Length != 10 || Id.ToLower()[0].ToString() != Prefix.ToLower();
            long result;
            if (flag)
            {
                result = 0L;
            }
            else
            {
                int num = 0;
                bool flag2 = !int.TryParse(Id[9].ToString(), out num);
                if (flag2)
                {
                    result = 0L;
                }
                else
                {
                    string text = Id.Substring(1, 8);
                    int num2 = 0;
                    int num3 = 0;
                    for (int i = 1; i <= text.Length; i++)
                    {
                        bool flag3 = int.TryParse(text[i - 1].ToString(), out num3);
                        if (!flag3)
                        {
                            result = 0L;
                            return result;
                        }
                        num2 += num3 * i;
                    }
                    bool flag4 = num2 % 9 != num;
                    if (flag4)
                    {
                        result = 0L;
                    }
                    else
                    {
                        string s = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}", new object[]
                        {
                            9 - int.Parse(text[7].ToString()),
                            9 - int.Parse(text[5].ToString()),
                            9 - int.Parse(text[6].ToString()),
                            9 - int.Parse(text[3].ToString()),
                            9 - int.Parse(text[4].ToString()),
                            9 - int.Parse(text[1].ToString()),
                            9 - int.Parse(text[2].ToString()),
                            9 - int.Parse(text[0].ToString())
                        });
                        long num4 = 0L;
                        bool flag5 = !long.TryParse(s, out num4);
                        if (flag5)
                        {
                            result = 0L;
                        }
                        else
                        {
                            result = num4;
                        }
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// 词库id转化产品url
        /// </summary>
        /// <param name="dctionaryId"></param>
        /// <returns></returns>
        public static string ToChanPinUrl(long dctionaryId)
        {
            return EncodeChanpinCiId((int)dctionaryId).Replace("W", "") + "/";
        }
        public static int ToCasDicId(string value)
        {
            if (!value.StartsWith("W")) value = "W" + value;
            return DecodeChanpinCiId(value);
        }

    }
}
