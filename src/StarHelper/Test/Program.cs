﻿// See https://aka.ms/new-console-template for more information

//using MongoDB.Entities;
//using StackExchange.Redis;
using com.epam.indigo;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.Drawing;

Console.WriteLine("Hello, World!");




//var timestampWithCheck = YYFUtils.TimestampWithCheck.GenerateTimestampWithCheck();
//Console.WriteLine("生成带校验的时间戳： " + timestampWithCheck);
//var isValid = YYFUtils.TimestampWithCheck.ValidateTimestamp(timestampWithCheck);
//Console.WriteLine("时间戳校验是否通过： " + (isValid ? "通过" : "不通过"));
//Console.ReadKey();

#region Indigo
Console.WriteLine("Hello, Indigo!");
IndigoNetImgHelper.ChemMolecule chemMolecule = new IndigoNetImgHelper.ChemMolecule(605, 375, 5);
//IndigoNetHelper.ChemMolecule chemMolecule = new IndigoNetHelper.ChemMolecule(500);
string path = "";
string errmsg = "";
string timemsg = "";
while (true)
{
    Console.WriteLine("输入Smiles文件");
    var smiles = Console.ReadLine();
    if (string.IsNullOrWhiteSpace(smiles))
    {
        smiles = "O=C1C2C(=CC=CC=2)CC2C1=CC=CC=2";
    }
    path = AppDomain.CurrentDomain.BaseDirectory + "images\\";
    if (!System.IO.Directory.Exists(path))
    {
        System.IO.Directory.CreateDirectory(path);
    }
    path += $"structimg_{DateTime.Now.ToString("yyyyMMddHHmmss")}.png";
    var wk = AppDomain.CurrentDomain.BaseDirectory + "logo.png";
    var bytes = chemMolecule.SaveToImageByte(smiles, out errmsg, out timemsg, 1, wk);
    using (var image = SixLabors.ImageSharp.Image.Load(bytes))
    {
        image.Save(path, new PngEncoder());
    }
    Dictionary<int, int> othersize = new Dictionary<int, int>();
    //othersize.Add(605, 375);
    othersize.Add(242, 150);
    othersize.Add(121, 75);
    foreach (var item in othersize)
    {
        using (var image = SixLabors.ImageSharp.Image.Load(bytes))
        {
            // 缩放图片到指定尺寸
            image.Mutate(ctx => ctx.Resize(new SixLabors.ImageSharp.Size(item.Key, item.Value)));
            path = path.Replace("structimg_", $"structimg_{item.Value}_");
            image.Save(path, new PngEncoder());
        }
    }
    //path = chemMolecule.SaveToImage(smiles, out errmsg, 500);
    Console.WriteLine("结果:" + path);
    Console.WriteLine("错误信息:" + errmsg);
    Console.WriteLine("耗时信息:" + timemsg);
}
#endregion

#region YYFRedis
//Console.WriteLine("Hello, YYFRedis!");
//var redisconnstr = "192.168.3.121,password=qwe123,connectTimeout=5000,defaultDatabase=10";
//redisconnstr = "192.168.3.121,syncTimeout=500000,abortConnect=false,password=qwe123,defaultDatabase=5";
//// 连接到Redis服务器
//var redis = new YYFRedis.RedisCacheProvider(redisconnstr);
//#region 临时消费程序
//Console.WriteLine("处理哪个站点的数据？ www/m/mip");
//var site = Console.ReadLine();
//if (string.IsNullOrWhiteSpace(site)) { site = "www"; }
//Console.Title = $"{site}站消费服务 < 平台页面访问消费程序";
//// 接收消息
//while (true)
//{
//    // 阻塞方式获取消息，无消息时会等待
//    var receivedMessage = redis.QueuePop($"visitlogqueue:{site}");
//    if (receivedMessage.IsNull)
//    {
//        // 没有消息可以处理，可以休眠一会儿
//        Console.WriteLine($"{site}站暂无数据，休息");
//        System.Threading.Thread.Sleep(1000);
//    }
//    else
//    {
//        Console.WriteLine($"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}]收到消息：{receivedMessage}");
//        // 处理消息的代码...
//    }
//}
#endregion

#region 实现消息队列功能
/*
var cango = true;
do
{
    Console.WriteLine("创建什么服务？1 生产者 2 消费者");
    var input = Console.ReadLine();
    if (input == "2")
    {
        Console.Title = "消费者(收消息) < 实现消息队列功能";

        // 接收消息
        while (true)
        {
            // 阻塞方式获取消息，无消息时会等待
            var receivedMessage = redis.QueuePop("yyfredis_queue");//redis.GetDatabase().ListLeftPop("yyfredis_queue");
            if (receivedMessage.IsNull)
            {
                // 没有消息可以处理，可以休眠一会儿
                Console.WriteLine($"休息");
                System.Threading.Thread.Sleep(1000);
            }
            else
            {
                Console.WriteLine($"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}]收到消息：{receivedMessage}");
                Console.WriteLine($"具体处理数据(费时2秒) todo");
                System.Threading.Thread.Sleep(2000);
                // 处理消息的代码...
            }
        }
    }
    else
    {
        Console.Title = "生产者(发消息) < 实现消息队列功能";
        Console.WriteLine("输入发布的信息内容：");
        input = Console.ReadLine();
        var msg = "";
        if (string.IsNullOrWhiteSpace(input))
        {
            for (int i = 1; i < 500; i++)
            {
                msg = $"[{i}]加入队列：{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}";
                var llp = redis.QueuePush("yyfredis_queue", msg);
                //var llp = redis.GetDatabase().ListLeftPush("yyfredis_queue", msg);
                Console.WriteLine($"当前队列数据量【{llp}】：{msg}");
                Thread.Sleep(1000);
            }
        }
        else
        {
            msg = $"[000]加入队列：{input}";
            redis.GetDatabase().ListLeftPush("yyfredis_queue", msg);
            Console.WriteLine(msg);
        }
    }
    Console.WriteLine("是否继续？");
    Console.ReadLine();
} while (cango);
*/
#endregion

#region 订阅发布
/*
Console.WriteLine("是否创建订阅服务？1是");
var input = Console.ReadLine();
if (input == "1")
{
    Console.Title = "订阅服务已开启";
    redis.Subscribe("yyfredis_channel", (channel, message) =>
    {
        Console.WriteLine($"订阅 Received message on {channel}: {message}");
    });
    // 等待订阅建立
    Console.WriteLine("等待订阅建立 Waiting for messages...");
    // 等待用户输入来退出程序
    Console.ReadLine();
}

Console.WriteLine("是否发布？0 否");
input = Console.ReadLine();
if (input != "0")
{
    Console.Title = "客户端已开启";
    var cango = true;
    do
    {
        Console.WriteLine("输入发布的信息内容：");
        input = Console.ReadLine();
        var c1 = redis.Publish("yyfredis_channel", $"发布信息：[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}]{input}");
        Console.WriteLine(c1 + "已发消息:" + input);
    } while (cango);
} 
*/
#endregion




/*
//StarMongoDbHelper
StarMongoDbHelper.DBHelper.DbConnStr = "mongodb://root:Kuujia2023$@192.168.1.47:27018";
StarMongoDbHelper.DBHelper.DbName = "kjwork";
StarMongoDbHelper.DBHelper dbhelper = new StarMongoDbHelper.DBHelper("", "");
var tmp = dbhelper.Get("users", "62b5a5ead600851fef1f0c07", new List<string>() { "username", "realname" }).Result;
Console.ReadKey();


var sif = new StarOSWatch.UseSysInfoWatch();
var status = Newtonsoft.Json.JsonConvert.SerializeObject(sif.GetServerStatus());
Console.WriteLine(status);
Console.ReadKey();
*/
Console.ReadKey();