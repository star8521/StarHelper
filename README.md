# StarHelper

#### 介绍
常用辅助类

目前支持：
敏感词过滤
时间戳转换
Email邮件发送
MD5
枚举帮助类
Html辅助类
Http请求辅助类
图片验证码生成
常见格式验证：email、手机号码、电话号码、QQ、中英文判断等
读写文件
XML序列化
Json序列化

基于MongoDB.Entities的MongoDB数据库辅助类，封装了常用方法：
获取列表数据、获取单条数据、通过过滤条件获取单条数据、新增修改数据、更新单条数据、根据条件批量更新数据、聚合查询、创建删除索引等


```
// 获取列表数据：
db.List("users",JObject.FromObject({page:1,psize:1000,filter:JSON.stringify({"_id":id}),fields:JSON.stringify(['_id'])})).Result
// 获取单条数据：
db.Get("users","xxxxxxx",fields).Result
// 通过过滤条件获取一条数据：
db.GetOne("users",JObject.FromObject({username:'yyfs'}),fields).Result
// 新增修改数据：
db.Save('users',JObject.FromObject(data),ret.optuser).Result
// 更新单条数据：
db.UpdateOne("users",JObject.FromObject({username:'yyfs'}),JObject.FromObject({status:0}),ret.optuser).Result;
// 更新数据：
db.Update("users",xxxxxxxx“”,JObject.FromObject({status:0}),ret.optuser).Result;
// 根据条件批量更新数据：
db.UpdateMany("users",JObject.FromObject({status:0}),JObject.FromObject({status:1}),ret.optuser).Result;

//聚合查询：
var data=[
    { $match: { "id":id }},
    { $unwind: "$cost" },
    {
        $group : {
            _id : "$_id",        //必须是_id
            cost: { $sum : "$cost"}    //cost 可以是任意值
        }
    }
];
db.aggregate('users',true,data).Result;

//创建删除索引：
db.CreateDbIndex("users","abc",[{"a":"aaa"},{"b":"bbb"}]) 
db.DropDbIndex("users","abc")

```



StarMongoDbHelper Demo:

```

class Program
{
    static void Main(string[] args)
    {
        var connectionString = "mongodb://mongodbuser:mongodbpwd@127.0.0.1:27017";
        var dbHelper = new StarMongoDbHelper.MongoDbHelper(connectionString, "demodb");
        var list = dbHelper.GetAllDocuments<DemoModel>("demo");

    }

    public class DemoModel
    {
        public ObjectId _id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
    }
}
```
